# Script to get unique snps from the pairs to be processed
# Paths are not set correctly yet
import gzip, json, os, time
DATAPATH = "/home/ggomez/Epistasia/data/input/"

# Save file of top snps by chromosome
def save_set_to_file(file, filename):

    snpdict = dict()
    for k in range(22):
        snpdict[str(k+1)] = list()

    for snp in file:
        snp = snp.split("_")
        ch = snp[0]
        pos = snp[1]

        # Save position to dict by chromosome
        snpdict[str(ch)].append(pos)

    with open(DATAPATH + "/pruned/clean_ids/" + filename + ".json", 'w') as fp:
        json.dump(snpdict, fp)

    del snpdict
    del file


# For every cohort in the path, save the top snps
for filename in os.listdir(DATAPATH + "pruned/ids"):
    cohortfile = os.path.join(DATAPATH + "pruned/ds", filename)
    
    print("Processing cohort " + filename)
    start_time = time.time()
    
    # Read file line by line
    snpsset = set()
    with open(cohortfile) as f:
        lines = f.readlines()
        nlines = 0

        for line in lines:
            
            # Discard header
            nlines+=1
            if nlines == 1:
                continue
                
            # Read lines and get position information
            lr = line.split("\t")
            snp1 = lr[0].split("_")
            snp2 = lr[1].split("_")

            ch1 = snp1[0]
            pos1 = snp1[1]

            ch2 = snp2[0]
            pos2 = snp2[1]

            snpsset.add(ch1+"_"+pos1)
            snpsset.add(ch2+"_"+pos2)
            
    save_set_to_file(snpsset, filename)

    print("--- %s seconds ---" % (time.time() - start_time))
    print("--- %s lines ---" % (nlines))
    print("--- %s snps unique ---" % (len(snpsset)))




