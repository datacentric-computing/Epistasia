#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=30:00:00
#SBATCH --output=logs/log01.out
#SBATCH --error=logs/log01.err
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

python get_uniques.py 

end=`date +%s`
runtime=$((end-start))
echo $runtime

