# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# This scripts process two files that must be indicated as an argument

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, happybase, gzip, os, csv
import numpy as np
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime

# PATHS
HDFSMASTER = "localhost"
HDFSPATH = "hdfs://"+HDFSMASTER+":9000/"
HOMEDATAPATH = "/gpfs/projects/bsc98/bsc98883/epistasis/data"
INPUTDATAPATH = "/gpfs/scratch/bsc98/bsc98883/epistasis/data/input/finalpairs_splited"

####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
init_time = now.strftime("%m%d%y%H%M")

# Default arguments
nPart = 6

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hc:k:"

# Long options
long_options = ["cohort", "keychromosome"]


try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-c, --cohort [string]: indicate the cohort to process.")
            print("-k, --keychromosome [int]: indicate the number of chromosome to process.")

            sys.exit()

        # Set the first file
        elif currentArgument in ("-c", "--cohort"):
            COHORT = currentValue
            print(("File is %s.") % currentValue)

        # Set the first file
        elif currentArgument in ("-k", "--keychromosome"):
            ikey = currentValue
            print(("Chromosome is %s.") % currentValue)

# Default values
except getopt.error as err:
    print("Please indicate the name of the cohort to process.")


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Vectorize function
imputfilter = np.vectorize(filter_imputation)

# Apply MDR to every SNP-SNP combination reading from a dict
def apply_mdr_zip(x):
    snp1 = x[0]
    snp2 = x[1]

    # Get key val from snp 1
    key1 = snp1[0]
    row1 = snp1[1]

    # Get key val from snp 2
    key2 = snp2[0]
    row2 = snp2[1]

    patients = transform_patients((row1,row2))

    testerror = get_risk_array(patients)

    return key1 +"--" + key2, testerror


# Apply MDR to every SNP-SNP combination reading from a dict
def apply_mdr_dict(x, rd1, rd2):
    key1 = x[0]
    key2 = x[1]

    if key1 in rd1:
        row1 = rd1[key1]
    else:
        return "ERROR", 0

    if key2 in rd2:
        row2 = rd2[key2]
    else:
        return "ERROR", 0

    patients = transform_patients((row1,row2))

    testerror = get_risk_array(patients)

    return key1 +"--" + key2, testerror

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):

    #1 - Transform to numpy arrays NO HBASE
    patients1 = np.array(x[0])
    patients2 = np.array(x[1])

    #2 - Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))
    
    #3 - Transform to integer
    pt1 = np.matmul(patientsdf1,np.transpose([1,2,3]))*3
    pt2 = np.matmul(patientsdf2,np.transpose([0,1,2]))
    ptcode = pt1-pt2

    return ptcode

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(patients):

    # Error list
    #trainerror = list()
    testerror = list()
    
    for i in range(5):
        
        # 1 - Get the sets for the iteration
        traindata = np.array(trainset[i])
        testdata = np.array(testset[i])
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)

        # 2 - Sum only the cases from training set
        cases = patients*npcases.T*traindata.T
        sumcases = count_occurrences(cases)

        # 2 - Sum only the controls
        controls = patients*npcontrols.T*traindata.T
        sumcontrols = count_occurrences(controls)

        # 3 - Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # 4 - Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # 5 - Classify training set
        prediction = apply_risk(patients, risk)

        # Get clasification error
        #trainerror.append((((npcases.T[0] == prediction)*traindata.T)[0]).sum()/Ntrain)

        # 6 - Get clasification error
        cv_testerror = (prediction + npcases.T[0])%2
        cv_testerror = (1-cv_testerror)*testdata.T
        error = int((cv_testerror.sum()/Ntest*100)*10)
        testerror.append(error)

    return testerror

# Transform the counts to an array
def count_occurrences(data):
    
    unique, counts = np.unique(data, return_counts=True)
    dtcounts = dict(zip(unique, counts))

    aux = list()
    for i in range(10):
        if i in dtcounts:
            aux.append(dtcounts[i])
        else:
            aux.append(0)
            
    return np.array(aux)

# Apply risk vector to classify the patients
def apply_risk(patients, risk):
    
    prediction = np.zeros(len(patients))
    casevalues = np.where(risk == 1)
    
    for n in casevalues[0][1:]:
        prediction[patients==n] = 1
        
    return prediction.astype(int)

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[2]
    val = imputfilter(aux[5:])

    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)

# Read file with top pairs info
def read_pairs(inputfile):
    top_pairs = dict()

    for i in range(22):
        for j in range(22):
            if i <= j:
                top_pairs[str(i+1)+'_'+str(j+1)] = list()

    nlines = 0
    with open(inputfile,'r') as finput:        
        for line in finput:
            
            # Discard header
            nlines+=1
            if nlines == 1:
                continue
                
            # Read lines and get position information
            lr = line.split("\t")
            snp1 = lr[0].split("_")
            snp2 = lr[1].split("_")

            ch1 = snp1[0]
            pos1 = snp1[1]

            ch2 = snp2[0]
            pos2 = snp2[1]

            if int(ch1) <= int(ch2):
                top_pairs[str(ch1)+'_'+str(ch2)].append([pos1, pos2])
            else:
                top_pairs[str(ch2)+'_'+str(ch1)].append([pos2, pos1])
    return top_pairs


# Read patients information
def cvsplit(cohort):
    # Read patients information from HDFS
    labels = spark.read.options(header = True, delimiter = " ").csv("file:///" + HOMEDATAPATH + "/input/labels/" + cohort + ".labels")

    print("Patients read")

    # Keep only case/control information
    for h in labels.columns:
        if h != "bin1":
            labels = labels.drop(h)

    # Filter first non relevant row
    labels = labels.filter("bin1!='B'")

    # Get np array with cases == 1
    npcases = np.array(labels.select("bin1").collect()).astype(int)

    # Get np array with controls == 1
    npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

    # Get cases/controls ratio. We will use the number as the high risk/low risk separator
    ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
    print("Ration cases/controls: " + str(ccratio[0]))

    # Create training and test set
    Npatients = labels.count()
    CV = 5
    block_size = Npatients/CV

    trainset = list()
    testset = list()

    print("Number of patients: ", Npatients)

    # Create two list with the train and test indexes
    for i in range(CV):
        nptrain = np.array([np.ones(Npatients, dtype=int)]).T
        nptrain[int(i*block_size):int(i*block_size + block_size)] = 0
        nptest = np.where((nptrain==0)|(nptrain==1), nptrain^1, nptrain)

        trainset.append(nptrain)
        testset.append(nptest)

    return trainset, testset, npcases, npcontrols, ccratio

# Divide list of pairs in chunks of a size
def divide_chunks(pairs, size):
     
    chunks = [pairs[i:i + size] for i in range(0, len(pairs), size)]
    return chunks

def read_blockfile(path):

    # Read file
    with gzip.open(path, 'rb') as f:
        lines = f.readlines()

    return lines

# Clean input data
def clean(x):
    aux = str(x).replace("b'", "").replace("\\n'","")
    return aux

# Get keys
def get_keys(x):
    return x[0]

################
##### MAIN #####
################

print("Starting main...")
starttime0 = timeit.default_timer()

conf = SparkConf()

# Configuration to work with 1 machine of 48 GB
conf.set("spark.executors.cores", "16")
conf.set("spark.total.excecutors", "3")
conf.set("spark.num.executors", "3")
conf.set("spark.driver.memory", "4g")
conf.set("spark.executor.memory", "16GB")
conf.setAppName('spark-basic')

sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

print("Connected with spartk")

trainset, testset, npcases, npcontrols, ccratio = cvsplit(COHORT)

print("Patients read")

pairsfile = HOMEDATAPATH + "/input/pruned/ids/" + COHORT + "_Epipair_list.txt"


# Read top pairs file
print("Loading file: " + pairsfile)
starttime = timeit.default_timer()

debugfile = list()

print("Combination to be processed: " + str(ikey))
key = ikey

CHUNKPATH = INPUTDATAPATH + "/" + COHORT + "/pairs_" + key + "_A/"
print("Number of 5000 pairs chunks to be process: " + str(len(os.listdir(CHUNKPATH))))

n = 0
for filename in os.listdir(CHUNKPATH):
    n += 1
   
    if os.path.isdir("/gpfs/scratch/bsc98/bsc98883/epistasis/data/output/output_" + COHORT + "/MDR_" + key + "_" + str(n)):
        print("File already exist. Skipping.")
        continue

    fileA = CHUNKPATH + filename
    fileB = fileA.replace("_A", "_B") 
    

    # Read files
    rdd1 = read_blockfile(fileA)
    rdd2 = read_blockfile(fileB)
  
    # Parallelize files
    rdd1 = sc.parallelize(rdd1, numSlices = 240)
    rdd2 = sc.parallelize(rdd2, numSlices = 240)

    # Clean lines
    rdd1 = rdd1.map(clean)
    rdd2 = rdd1.map(clean)

    # Remove blank lines
    rdd1 = rdd1.filter(bool)    
    rdd2 = rdd2.filter(bool) 

    # Convert into key-val rdd
    rdd1 = rdd1.map(get_keyval)
    rdd2 = rdd2.map(get_keyval)

    #lrd1 = rdd1.count()
    #rdd1dict = dict(rdd1.take(lrd1))
    #rdd2dict = dict(rdd2.take(lrd1))

    rddzip = rdd1.zip(rdd2)

    # Make pairs list
    #ipairs = list(zip(rdd1dict.keys(), rdd2dict.keys())) 
    #rddpairs = sc.parallelize(ipairs)

    # Compute MDR
    #mdrerror = rddpairs.map(lambda x: apply_mdr_dict(x, rdd1dict, rdd2dict))
    mdrerror = rddzip.map(apply_mdr_zip)

    print("Saving chunk " + str(n) + " results to HDFS...")
    output_dir =  "MDR_" + key + "_" + str(n)
    mdrerror.saveAsTextFile(HDFSPATH + "output_" + COHORT + "/" + output_dir, compressionCodecClass="org.apache.hadoop.io.compress.GzipCodec")

print("--------- Total time: " + str(timeit.default_timer()-starttime0) + " ---------")
print("-------------------------------------")

