#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=48:00:00
#SBATCH --output=logs/log04.out
#SBATCH --error=logs/log04.err
#SBATCH -N 2
#SBATCH -c 48


start=`date +%s`

#echo "Running NuGENE..."
#./04_make_partitions.sh NuGENE

end=`date +%s`
runtime=$((end-start))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GENEVA..."
./04_make_partitions.sh GENEVA

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running WTCCC..."
#./04_make_partitions.sh WTCCC

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GERA1..."
#./04_make_partitions.sh GERA1

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GERA2..."
#./04_make_partitions.sh GERA2

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

#echo "Running FUSION..."
#./04_make_partitions.sh FUSION

end=`date +%s`
runtime=$((end-start))
echo Total final secs: $runtime

