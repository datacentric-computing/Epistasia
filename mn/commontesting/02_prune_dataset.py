import gzip, json, os, time, sys

COHORTSPATH = "/gpfs/projects/bsc05/montse/epistasis/10_split_chrs/"
SNPSIDPATH = "/gpfs/projects/bsc98/bsc98883/epistasis/data/input/pruned/clean_ids/"
OUTPUTPATH = "/gpfs/projects/bsc98/bsc98883/epistasis/data/input/pruned/data/"

# For every cohort in the path, prun the datasets and keep only the interested snps

# Get arguments
cohort = sys.argv[1]
start_time = time.time()

with open(SNPSIDPATH + cohort + "_Epipair_list.txt.json", 'r') as fp:
    topsnps = json.load(fp)
    
    lefts = dict()
    for ch, values in topsnps.items():
        lefts[ch] = 0
        print("Processing chromosome " + str(ch))
        start_ch_time = time.time()

        # Make list with selected snps
        prunedsnps = list()
        for filename in os.listdir(COHORTSPATH + cohort + "/" + "chr" + ch):
            chr_part = os.path.join(COHORTSPATH + cohort + "/" + "chr" + ch, filename)
            print("Reading file " + chr_part)

            with gzip.open(chr_part, 'rb') as f1:
                for line in f1:
                    line = line.decode('utf-8')
                    sline = line.split()
                    pos = sline[2]
                    if pos in values:
                        prunedsnps.append(line)
                    else:
                        lefts[ch] +=1
                    
        outputfile = OUTPUTPATH + cohort + "/pruned_chr" + ch + ".txt"         
        with open(outputfile, 'w') as fp:
            fp.write('\n'.join(prunedsnps))
            
        print("--- %s lines ---" % (len(prunedsnps)))
        print("--- %s lefts ---" % (lefts[ch]))
        print("--- %s seconds ---" % (time.time() - start_ch_time))
        print("")

print("--- %s total seconds ---" % (time.time() - start_time))


