#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=48:00:00
#SBATCH --output=logs/log03.out
#SBATCH --error=logs/log03.err
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

echo "Running FUSION..."
#singularity run ~/epistasis/testing/images/standalone_mdr.sif python 03_get_pairs.py -f FUSION

end=`date +%s`
runtime=$((end-start))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GENEVA..."
#singularity run ~/epistasis/testing/images/standalone_mdr.sif python 03_get_pairs.py -f GENEVA

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running WTCCC..."
#python 03_get_pairs.py -f WTCCC

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GERA1..."
singularity run ~/epistasis/testing/images/standalone_mdr.sif python 03_get_pairs.py -f GERA1

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running GERA2..."
#python 03_get_pairs.py -f GERA2

end=`date +%s`
runtime=$((end-start2))
echo Total secs: $runtime
start2=`date +%s`

echo "Running NuGene..."
#python 03_get_pairs.py -f NuGENE


end=`date +%s`
runtime=$((end-start))
echo Total final secs: $runtime

