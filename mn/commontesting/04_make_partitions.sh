COHORT=$1

INPUTPATH=/gpfs/scratch/bsc98/bsc98883/epistasis/data/input/finalpairs/$COHORT
OUTPUTPATH=/gpfs/scratch/bsc98/bsc98883/epistasis/data/input/finalpairs_splited/$COHORT

for file in $INPUTPATH/*; do

    txfile=${file%.*}
    rawfile=${txfile##*/}
    rawfile=${rawfile%.*} 

    echo Processing file $file
    echo Output folder: $OUTPUTPATH/$rawfile
    #echo rawfile - $rawfile
    #echo compress - $OUTPUTPATH/$rawfile

    if test -d $OUTPUTPATH/$rawfile; then
        echo "Splitted files exist, skipping it."
    else
        echo "Splited files not found, creating them..."
        
        gzip -d -k $file
        mkdir $OUTPUTPATH/$rawfile
        split -l 5000 -d $txfile $OUTPUTPATH/$rawfile/$rawfile
        gzip $OUTPUTPATH/$rawfile/*    
        find $OUTPUTPATH/$rawfile/ -not -name  '*.gz' -type f -delete
        rm $txfile
    fi

    echo -------- FILE PROCESSED --------
    echo --------------------------------

done
