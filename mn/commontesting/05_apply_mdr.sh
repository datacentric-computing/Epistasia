#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=24:00:00
#SBATCH --output=logs/log05.out
#SBATCH --error=logs/log05.err
#SBATCH -N 2
#SBATCH -c 48

start=`date +%s`

echo "Init instance"
./standalone_start.sh

# SET COHORT
COHORT=GENEVA

# SET PATHS
DATAHOME=/gpfs/scratch/bsc98/bsc98883/epistasis/data
PAIRS=$DATAHOME/input/pruned/ids/${COHORT}_Epipair_list.txt # This file contains the pairs that must be processed

singularity exec instance://standalone_mdr hdfs dfs -mkdir /output_$COHORT

echo "Using list of paris '$PAIRS'"

for i in {1..22}
do
    for j in {1..22}
    do
    
        echo "Processing file '$i' and '$j'..."  

        echo "Running python script with singularity..."
        singularity run ~/epistasis/testing/images/standalone_mdr.sif python 05_apply_mdr.py -c $COHORT -k ${i}_$j

        echo "Printing HDFS output directory..."
        singularity exec instance://standalone_mdr hdfs dfs -ls /output_$COHORT > hdfslog/hdfsoutput.out

        echo "Downloading HDFS output data..."
        singularity exec instance://standalone_mdr hdfs dfs -get /output_$COHORT/ $DATAHOME/output/
        singularity exec instance://standalone_mdr hdfs dfs -rm -r /output_$COHORT/*

    done

    end=`date +%s`
    runtime=$((end-start))
    echo "Chromosome '$i' processed in '$runtime' seconds."

done

echo "Closing everything"
./standalone_stop.sh

end=`date +%s`
runtime=$((end-start))
echo "Total time: '$runtime' seconds."


