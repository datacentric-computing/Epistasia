# Get top pairs combinations ready to process from list of them
# By Gonzalo Gomez-Sanchez. 

# This scripts process two files that must be indicated as an argument

# IMPORTS
import time, timeit, sys, gzip, getopt, os

# PATHS
PROJECTPATH = "/gpfs/projects/bsc98/bsc98883/epistasis"
OUTPUTPATH = "/gpfs/scratch/bsc98/bsc98883/epistasis"

####################
#### ARGUMENTS #####
####################

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hf:"

# Long options
long_options = ["file"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-f, --file [string]: indicate the first file.")
            sys.exit()

        # Set the first file
        elif currentArgument in ("-f", "--file"):
            cohort = currentValue
            print(("File is %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Please indicate the cohort.")


#####################
##### FUNCTIONS #####
#####################


# Read data of common pairs into dict
def readpairs(filepath):

    common_pairs = dict()
    # Create dict with all ch combinations
    for i in range(22):
        for j in range(22):
            if i <= j:
               common_pairs[str(i+1)+'_'+str(j+1)] = list()

    # Read top pairs file
    with open(filepath) as f:        

        lines = f.readlines()
        nlines = 0
        # Decode every line
        for line in lines:
            nlines +=1
            if nlines ==1:
                continue
                
            lr = line.split("\t")
            
            snp1 = lr[0].split("_")
            snp2 = lr[1].split("_")

            ch1 = snp1[0]
            pos1 = snp1[1]

            ch2 = snp2[0]
            pos2 = snp2[1]

            # Save information into dict
            if int(ch1) <= int(ch2):
                common_pairs[str(ch1)+'_'+str(ch2)].append([pos1, pos2])
            else:
                common_pairs[str(ch2)+'_'+str(ch1)].append([pos2, pos1])

    return common_pairs


################
##### MAIN #####
################

print("Starting main...")
starttime = timeit.default_timer()

# Compute top pairs  
print("Loading file: " + cohort)

# Read file with pairs to compute - {CHRA_CHB : [pos1, pos2], [pos3, pos4]...; ..}
common_pairs = readpairs(PROJECTPATH + "/data/input/pruned/ids/" + cohort + "_Epipair_list.txt")

# Processing all combinations
for k in common_pairs.keys():

    print("Processing key : " + str(k))
 
    # Get Chromosomes
    chs = k.split("_")
    chA = chs[0]
    chB = chs[1]

    # Save data of chromosome A
    CHAFILE = OUTPUTPATH + "/data/input/finalpairs/" + cohort + "/pairs_" + chA + "_" + chB + "_A.txt"

    with open(PROJECTPATH + "/commontesting/pylog.out", 'a') as f:
        f.write("Processing " + str(chA) + " vs " + str(chB) + "\n")

    # Check if file does already exist
    if not os.path.isfile(CHAFILE + ".gz"):

        with open(PROJECTPATH + "/commontesting/pylog.out", 'a') as f:
            f.write("File to be created: " + CHAFILE + ".gz\n")        

        with open(PROJECTPATH + "/data/input/pruned/cohorts/" + cohort + "/pruned_chr" + chA + ".txt", 'r') as f1:

            # Get dict with chromosome data
            cohort_data = dict()
            for line in f1:
                sline = line.split()
                if len(sline) > 2:
                    pos = sline[2]
                    cohort_data[pos] = line

            # Save snps and information sorted
            with open(CHAFILE, 'w') as fp:
                for pp in common_pairs[k]:
                    fp.write("%s\n" % cohort_data[pp[0]])

            cmd = "gzip " + CHAFILE
            os.system(cmd) 

    else:
        with open(PROJECTPATH + "/commontesting/pylog.out", 'a') as f:
            f.write("Skipping A file\n")

    # Save data of chromosome B
    CHBFILE = OUTPUTPATH + "/data/input/finalpairs/" + cohort + "/pairs_" + chA + "_" + chB + "_B.txt"

    # Check if file does already exist
    if not os.path.isfile(CHBFILE + ".gz"):        
        with open(PROJECTPATH + "/data/input/pruned/cohorts/" + cohort + "/pruned_chr" + chB + ".txt", 'r') as f1:

            # Get dict with chromosome data
            cohort_data = dict()
            for line in f1:
                sline = line.split()
                if len(sline) > 2:
                    pos = sline[2]
                    cohort_data[pos] = line
 
            # Save snps and information sorted
            with open(CHBFILE, 'w') as fp:
                for pp in common_pairs[k]:
                    fp.write("%s\n" % cohort_data[pp[1]])

            cmd = "gzip " + CHBFILE
            os.system(cmd) 

    else:
        with open(PROJECTPATH + "/commontesting/pylog.out", 'a') as f:
            f.write("Skipping B file\n")

    with open(PROJECTPATH + "/commontesting/pylog.out", 'a') as f:
        f.write("Combination time of " + cohort + " for chromosome "  + chA + " and " + chB + " was "  + str(timeit.default_timer()-starttime)+ "\n")

