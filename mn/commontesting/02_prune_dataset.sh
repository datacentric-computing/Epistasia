#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=30:00:00
#SBATCH --output=logs/log02.out
#SBATCH --error=logs/log02.err
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

# Remove the comment of the cohorts you want to process

#echo "Processing cohort NuGENE"
#python prune_dataset.py NuGENE

#echo "Processing cohort FUSION"
#python prune_dataset.py FUSION

#echo "Processing cohort WTCCC"
#python prune_dataset.py WTCCC

#echo "Processing cohort GENEVA"
#python prune_dataset.py GENEVA

#echo "Processing cohort GERA1"
#python prune_dataset.py GERA1

echo "Processing cohort GERA2"
python prune_dataset.py GERA2

end=`date +%s`
runtime=$((end-start))
echo $runtime

