#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=10:00:00
#SBATCH --output=logs/log.out
#SBATCH --error=logs/log.err
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

echo "Init instance"
./standalone_start.sh

# SET PATHS
DATAHOME=/gpfs/projects/bsc98/bsc98883/epistasis/data
PAIRS=$DATAHOME/input/NuGENE_top_pairs.txt.gz

singularity exec instance://standalone_mdr hdfs dfs -mkdir /input
singularity exec instance://standalone_mdr hdfs dfs -mkdir /output


for i in {1..22}
do
    
    INPUTDATA=$DATAHOME/input/toppairs/$i
    singularity exec instance://standalone_mdr hdfs dfs -put $INPUTDATA /input/

    echo "Printing HDFS input directory..."
    singularity exec instance://standalone_mdr hdfs dfs -ls /input

    echo "Running python script with singularity..."
    singularity run ~/epistasis/testing/images/standalone_mdr.sif python mdr_toppairs.py -f $PAIRS -k $i

    echo "Printing HDFS output directory..."
    singularity exec instance://standalone_mdr hdfs dfs -ls /output

    echo "Downloading HDFS output data..."
    singularity exec instance://standalone_mdr hdfs dfs -get /output $DATAHOME/output/

    echo "Removing data form HDFS input..."
    singularity exec instance://standalone_mdr hdfs dfs -rm -r /input/*

done

echo "Closing everything"
./standalone_stop.sh

end=`date +%s`
runtime=$((end-start))
echo $runtime

