# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# This scripts process two files that must be indicated as an argument

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, happybase, gzip, os
import numpy as np
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime

# PATHS
HDFSMASTER = "localhost"
HDFSPATH = "hdfs://"+HDFSMASTER+":9000/"
HOMEDATAPATH = "/gpfs/projects/bsc98/bsc98883/epistasis/data"

####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
init_time = now.strftime("%m%d%y%H%M")

# Default arguments
nPart = 6

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hf:k:"

# Long options
long_options = ["help", "file", "key"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-f, --file [string]: indicate the file of the top pairs.")
            print("-k, --key [string]: indicate the first number of keys to process.")
            sys.exit()

        # Set the first file
        elif currentArgument in ("-f", "--file"):
            pairsfile = currentValue
            print(("Pairs file is %s.") % currentValue)


        elif currentArgument in ("-k", "--key"):
            ikey = currentValue
            print(("Key is %s.") % currentValue)

# Default values
except getopt.error as err:
    print("Please indicate the name of the file.")


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Vectorize function
imputfilter = np.vectorize(filter_imputation)

# Apply MDR to every SNP-SNP combination reading from a dict
def apply_mdr_dict(x, rd1, rd2):
    key1 = x[0]
    key2 = x[1]
    row1 = rd1[key1]
    row2 = rd2[key2]

    patients = transform_patients((row1,row2))

    testerror = get_risk_array(patients)

    return key1 +"--" + key2, testerror

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):

    #1 - Transform to numpy arrays NO HBASE
    patients1 = np.array(x[0])
    patients2 = np.array(x[1])

    #2 - Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))
    
    #3 - Transform to integer
    pt1 = np.matmul(patientsdf1,np.transpose([1,2,3]))*3
    pt2 = np.matmul(patientsdf2,np.transpose([0,1,2]))
    ptcode = pt1-pt2

    return ptcode

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(patients):

    # Error list
    #trainerror = list()
    testerror = list()
    
    for i in range(10):
        
        # 1 - Get the sets for the iteration
        traindata = np.array(trainset[i])
        testdata = np.array(testset[i])
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)

        # 2 - Sum only the cases from training set
        cases = patients*npcases.T*traindata.T
        sumcases = count_occurrences(cases)

        # 2 - Sum only the controls
        controls = patients*npcontrols.T*traindata.T
        sumcontrols = count_occurrences(controls)

        # 3 - Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # 4 - Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # 5 - Classify training set
        prediction = apply_risk(patients, risk)

        # Get clasification error
        #trainerror.append((((npcases.T[0] == prediction)*traindata.T)[0]).sum()/Ntrain)

        # 6 - Get clasification error
        cv_testerror = (prediction + npcases.T[0])%2
        cv_testerror = (1-cv_testerror)*testdata.T
        error = int((cv_testerror.sum()/Ntest*100)*10)
        testerror.append(error)

    return testerror

# Transform the counts to an array
def count_occurrences(data):
    
    unique, counts = np.unique(data, return_counts=True)
    dtcounts = dict(zip(unique, counts))

    aux = list()
    for i in range(10):
        if i in dtcounts:
            aux.append(dtcounts[i])
        else:
            aux.append(0)
            
    return np.array(aux)

# Apply risk vector to classify the patients
def apply_risk(patients, risk):
    
    prediction = np.zeros(len(patients))
    casevalues = np.where(risk == 1)
    
    for n in casevalues[0][1:]:
        prediction[patients==n] = 1
        
    return prediction.astype(int)

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[2]
    val = imputfilter(aux[5:])
    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)

# Read file with top pairs info
def read_pairs(inputfile):
    top_pairs = dict()

    for i in range(22):
        for j in range(22):
            if i <= j:
                top_pairs[str(i+1)+'_'+str(j+1)] = list()

    nlines = 0
    with gzip.open(inputfile,'rb') as finput:        
        for line in finput:
            lr = line.decode("utf-8").split("\t")
            ch1 = lr[0]
            ch2 = lr[3]
            pos1 = lr[1]
            pos2 = lr[4]
            if int(ch1) <= int(ch2):
                top_pairs[str(ch1)+'_'+str(ch2)].append([pos1, pos2])
            else:
                top_pairs[str(ch2)+'_'+str(ch1)].append([pos2, pos1])
    return top_pairs

################
##### MAIN #####
################

print("Starting main...")
starttime0 = timeit.default_timer()

conf = SparkConf()
#conf.set("spark.executors.cores", "4")
conf.set("spark.num.executors", "3")
conf.set("spark.executor.memory", "32GB")
conf.setAppName('spark-basic')

sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

print("Connected with spartk")

# Read patients information from HDFS
#labels = spark.read.options(header = True, delimiter = " ").csv(HDFSPATH + "/input/5CVSETS.npy") 
labels = np.load(HOMEDATAPATH + "/input/10CVSETS.npy", allow_pickle=True)
trainset = labels[0]
testset = labels[1]
npcases = labels[2]
npcontrols = labels[3]
ccratio = labels[4]

print("Patients read")

# Read top pairs file
print("Loading file: " + pairsfile)
starttime = timeit.default_timer()

top_pairs = read_pairs(pairsfile)

for key, pairs in top_pairs.items():

    if key.startswith(ikey) == False:
        continue

    DATAPATH1 = HOMEDATAPATH + "/input/toppairs/NuGENEtop_"+key+"_A.txt"
    DATAPATH2 = HOMEDATAPATH + "/input/toppairs/NuGENEtop_"+key+"_B.txt"

    print("Processing file NuGENEtop_"+key)
    print("Number of pairs to process: " + str(len(pairs)))
    file1 = "NuGENEtop_"+key+"_A.txt"
    file2 = "NuGENEtop_"+key+"_B.txt"        

    # Read file 1
    rdd1 = spark.sparkContext.textFile(HDFSPATH + "/input/"+ ikey + "/" + file1)
    rdd2 = spark.sparkContext.textFile(HDFSPATH +"/input/" + ikey + "/" +  file2)

    # Remove blank lines
    rdd1 = rdd1.filter(bool)    
    rdd2 = rdd2.filter(bool)

    # Convert into key-val rdd
    rdd1 = rdd1.map(get_keyval)
    rdd2 = rdd2.map(get_keyval)

    # Get as dict
    rdd1dict = rdd1.collectAsMap()
    rdd2dict = rdd2.collectAsMap()

    # Parallelize pairs to process
    rddpairs = sc.parallelize(pairs)

    # Compute MDR
    mdrerror = rddpairs.map(lambda x: apply_mdr_dict(x, rdd1dict, rdd2dict))

    print("Saving data to HDFS...")
    output_dir =  "MDR_" + key
    mdrerror.saveAsTextFile(HDFSPATH + "output/" + output_dir, compressionCodecClass="org.apache.hadoop.io.compress.GzipCodec")

    cmd = 'singularity exec instance://standalone_mdr hdfs dfs -rm /input/' + file1
    os.system(cmd)

    cmd = 'singularity exec instance://standalone_mdr hdfs dfs -rm /input/' + file2
    os.system(cmd)

print("Total time: ", timeit.default_timer()-starttime0)
