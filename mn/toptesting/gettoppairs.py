# Get top pairs combinations ready to process from list of them
# By Gonzalo Gomez-Sanchez. 

# This scripts process two files that must be indicated as an argument

# IMPORTS
import time, timeit, sys, gzip, getopt

# PATHS
PROJECTPATH = "/gpfs/projects/bsc98/bsc98883/epistasis"

####################
#### ARGUMENTS #####
####################

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hf:"

# Long options
long_options = ["file"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-f, --file [string]: indicate the first file.")
            sys.exit()

        # Set the first file
        elif currentArgument in ("-f", "--file"):
            topfile = currentValue
            print(("File is %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Please indicate the number of files.")


#####################
##### FUNCTIONS #####
#####################


# Read data of top pairs into dict
def readtoppairs(filepath):

    top_pairs = dict()
    # Create dict with all ch combinations
    for i in range(22):
        for j in range(22):
            if i <= j:
                top_pairs[str(i+1)+'_'+str(j+1)] = list()

    # Read top pairs file
    with gzip.open(filepath,'rb') as finput:        
        # Decode every line
        for line in finput:
            lr = line.decode("utf-8").split("\t")
            ch1 = lr[0]
            ch2 = lr[3]
            pos1 = lr[1]
            pos2 = lr[4]
            chi = lr[6]

            # Save information into dict
            if int(ch1) <= int(ch2):
                top_pairs[str(ch1)+'_'+str(ch2)].append([pos1, pos2, chi])
            else:
                top_pairs[str(ch2)+'_'+str(ch1)].append([pos2, pos1, chi])

    return top_pairs


################
##### MAIN #####
################

print("Starting main...")
starttime = timeit.default_timer()

# Compute top pairs  
print("Loading file: " + topfile)

# Read file with pairs to compute
top_pairs = readtoppairs(topfile)

# Processing all combinations
for k in top_pairs.keys():
    snpsA = dict()
    snpsB = dict()

    # For combination, get list of snps for each chromosome
    for pp in top_pairs[k]:
        if pp[0] not in snpsA.keys():
            snpsA[pp[0]] = 0
        if pp[1] not in snpsB.keys():
            snpsB[pp[1]] = 0
 

    # Get Chromosomes
    chs = k.split("_")
    chA = chs[0]
    chB = chs[1]

    # Get data of chromosome 1
    CHAFILE = PROJECTPATH + "/toptesting/toppairs/input/NuGENEtop_" + chA + "_" + chB + "_A.txt"
    with gzip.open(PROJECTPATH + "/input/NuGENE_chr" + chA + "_repaired.gz", 'rb') as f1:
        with open(CHAFILE, 'w') as fp:
            for line in f1:
                line = line.decode('utf-8')
                sline = line.split()
                ch = sline[0]
                pos = sline[2]
                if pos in snpsA.keys():
                    wline = line
                    fp.write("%s\n" % wline)

    CHBFILE = PROJECTPATH + "/toptesting/toppairs/input/NuGENEtop_" + chA + "_" + chB + "_B.txt"
    with gzip.open(PROJECTPATH + "/input/NuGENE_chr" + chB + "_repaired.gz", 'rb') as f1:
        with open(CHBFILE, 'w') as fp:
            for line in f1:
                line = line.decode('utf-8')
                sline = line.split()
                ch = sline[0]
                pos = sline[2]
                if pos in snpsB.keys():
                    wline = line
                    fp.write("%s\n" % wline)

    with open(PROJECTPATH + "/toptesting/pylog.out", 'w') as f:
        f.write("Combination time of " + chA + " and " + chB + " was "  + str(timeit.default_timer()-starttime)+ "\n")

