#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=24:00:00
#SBATCH --output=log.out
#SBATCH --error=log.err
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

echo "Init instance"
./standalone_start.sh

# SET PATHS
PAIRS=/gpfs/projects/bsc98/bsc98883/epistasis/toptesting/NuGENE_top_pairs.txt.gz

echo "Running python script with singularity..."
singularity run ~/epistasis/testing/images/standalone_mdr.sif python mdr_toppairs.py -f $PAIRS

echo "Closing everything"
./standalone_stop.sh

end=`date +%s`
runtime=$((end-start))
echo $runtime

