# Create instance and init HDFS

# Make sure to clean the tmp directory
# Create logs directories
rm -r /tmp/*
mkdir -p /tmp/hdfs/namenode
mkdir -p /tmp/hdfs/datanode


# Create instances
singularity instance start ../images/standalone_mdr.sif standalone_mdr

# Format HDFS
singularity exec instance://standalone_mdr hdfs namenode -format

# Init HDFS
singularity exec instance://standalone_mdr hdfs --daemon start namenode
singularity exec instance://standalone_mdr yarn --daemon start resourcemanager
singularity exec instance://standalone_mdr hdfs --daemon start datanode
singularity exec instance://standalone_mdr yarn --daemon start nodemanager

