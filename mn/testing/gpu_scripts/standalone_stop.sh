# Create instance and init HDFS

# Set Master
MASTER=bscdc-gpu1

# Init HDFS
singularity exec instance://standalone_mdr hdfs --daemon stop namenode
singularity exec instance://standalone_mdr yarn --daemon stop resourcemanager
singularity exec instance://standalone_mdr hdfs --daemon stop datanode
singularity exec instance://standalone_mdr yarn --daemon stop nodemanager


# Create instances
singularity instance stop standalone_mdr


