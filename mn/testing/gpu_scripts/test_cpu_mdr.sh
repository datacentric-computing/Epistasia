#!/bin/bash
#SBATCH --qos=debug
#SBATCH --time=02:00:00
#SBATCH --error=outputs/test_output.stderr
#SBATCH --output=outputs/test_output.stdout
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

echo "Init instance"
# Init instance and HDFS
./standalone_start.sh
end=`date +%s`
runtime=$((end-start))
echo $runtime

echo "Running script"
# Run script
while read line1; do
    while read line2; do
    ./cpu_run.sh $line1 $line2
    done < file2.txt
done < file1.txt

end=`date +%s`
runtime=$((end-start))
echo $runtime

echo "Downloading data"
# Download data
singularity exec instance://standalone_mdr hdfs dfs -get /output/* output/*
end=`date +%s`
runtime=$((end-start))
echo $runtime

echo "Closing everything"
# Stop HDFS and instance
./standalone_stop.sh
end=`date +%s`
runtime=$((end-start))
echo $runtime
