# Set Master
DATA=~/data/input/split_data/*
HEADER=~/data/input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample

singularity exec instance://standalone_mdr hdfs dfs -mkdir /input
singularity exec instance://standalone_mdr hdfs dfs -mkdir /output

singularity exec instance://standalone_mdr hdfs dfs -put $DATA /input
singularity exec instance://standalone_mdr hdfs dfs -put $HEADER /input

singularity run --nv ~/standalone_mdr.sif python gpu_mdr.py
