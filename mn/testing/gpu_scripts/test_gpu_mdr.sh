#!/bin/bash
#SBATCH --time=00:05:00
#SBATCH --error=test_output.stderr
#SBATCH --output=test_output.stdout

# Init instance and HDFS
./standalone_start.sh

# Run script
./gpu_run.sh

# Download data
singularity exec instance://standalone_mdr hdfs dfs -get /output /tmp/output

# Stop HDFS and instance
./standalone_stop.sh
