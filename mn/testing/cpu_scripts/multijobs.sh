if [[ $# -eq 0 ]] ; then
    echo 'Please introduce the number of combinations files to process'
    exit 1
fi

C=$1
n=0
while read line
do
    # Throw job
    echo "Line No. $n : $line"
    sbatch --output logs/${line}.out --error logs/${line}.err test_cpu_mdr.sh $line
    n=$((n+1))

    # Stop if number of jobs achieve
    if [[ "$n" == "$C" ]]
        then
                echo "Number $n, stopping"
                break
        fi

done < combinations.txt

head -n +$C combinations.txt >> done_combinations.txt
tail -n +$(($C+1)) combinations.txt > combinations2.txt
rm combinations.txt
mv combinations2.txt combinations.txt



