#!/bin/sh  
while true
do
  # Count number of jobs running
  aux=$(squeue | wc -l)
  echo There are ${aux} jobs running.

  # Check if there is less jobs than allowed
  if (("$aux" < "365"))
  then
     # Throw more jobs
     njobs=$((365-$aux))
     echo Throwing $njobs jobs
     ./multijobs.sh $njobs
  else
    echo Max jobs running.
  fi

  # Wait 5 minutes before running again
  sleep 300
done 

