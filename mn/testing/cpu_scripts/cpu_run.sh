# Get folders
IFS='_' 
read -ra split1 <<< "$1"
read -ra split2 <<< "$2"
IFS=''

folder1=${split1[0]}_${split1[1]}
folder2=${split2[0]}_${split2[1]}

# SET PATHS
DATA1=~/epistasis/data/input/split_data/partitions/$folder1/$1
DATA2=~/epistasis/data/input/split_data/partitions/$folder2/$2
#HEADER=~/epistasis/data/input/5CVSETS.npy

echo "CPURUN - Loading data to HDFS..."

singularity exec instance://standalone_mdr_$3 hdfs dfs -mkdir /input
singularity exec instance://standalone_mdr_$3 hdfs dfs -mkdir /output

singularity exec instance://standalone_mdr_$3 hdfs dfs -put $DATA1 /input
singularity exec instance://standalone_mdr_$3 hdfs dfs -put $DATA2 /input
#singularity exec instance://standalone_mdr hdfs dfs -put $HEADER /input

echo "CPURUN - Printing input folder"
singularity exec instance://standalone_mdr_$3 hdfs dfs -ls /input

echo "CPURUN - Running python script with singularity..."
singularity run ~/epistasis/testing/images/standalone_mdr.sif python throw_cpu_mdr.py -f $1 -s $2 -c $3

echo "CPURUN - Printing output folder"
singularity exec instance://standalone_mdr_$3 hdfs dfs -ls /output


