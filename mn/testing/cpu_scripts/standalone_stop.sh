# Create instance and init HDFS

# Init HDFS
singularity exec instance://standalone_mdr_$1 hdfs --daemon stop namenode
singularity exec instance://standalone_mdr_$1 yarn --daemon stop resourcemanager
singularity exec instance://standalone_mdr_$1 hdfs --daemon stop datanode
singularity exec instance://standalone_mdr_$1 yarn --daemon stop nodemanager


# Create instances
singularity instance stop standalone_mdr_$1


