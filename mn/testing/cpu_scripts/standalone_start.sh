# Create instance and init HDFS

# Make sure to clean the tmp directory
# Create logs directories
rm -r -f /tmp/*
mkdir -p /tmp/hdfs/namenode
mkdir -p /tmp/hdfs/datanode


# Create instances
singularity instance start ../images/standalone_mdr.sif standalone_mdr_$1

# Format HDFS
echo "Y" | singularity exec instance://standalone_mdr_$1 hdfs namenode -format

# Init HDFS
singularity exec instance://standalone_mdr_$1 hdfs --daemon start namenode
singularity exec instance://standalone_mdr_$1 yarn --daemon start resourcemanager
singularity exec instance://standalone_mdr_$1 hdfs --daemon start datanode
singularity exec instance://standalone_mdr_$1 yarn --daemon start nodemanager

