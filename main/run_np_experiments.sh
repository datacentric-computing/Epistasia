cores=$1
workers=$2

# Perform the experiments with the desired number of files for testing. 
# Take into account that 10 files is 10²/2 combinations and 50 is 50²/2 combinations.

singularity exec instance://ihbsp hdfs dfs -rm -r /output/*
singularity exec instance://ihbsp python sparkmdr_hdfs.py -f 10 -c $cores -w $workers

singularity exec instance://ihbsp hdfs dfs -rm -r /output/*
singularity exec instance://ihbsp python sparkmdr_hdfs.py -f 20 -c $cores -w $workers

singularity exec instance://ihbsp hdfs dfs -rm -r /output/*
singularity exec instance://ihbsp python sparkmdr_hdfs.py -f 30 -c $cores -w $workers

singularity exec instance://ihbsp hdfs dfs -rm -r /output/*
singularity exec instance://ihbsp python sparkmdr_hdfs.py -f 40 -c $cores -w $workers

singularity exec instance://ihbsp hdfs dfs -rm -r /output/*
singularity exec instance://ihbsp python sparkmdr_hdfs.py -f 50 -c $cores -w $workers


