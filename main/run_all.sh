# Indicate the number of CORES per worker
CORES=8

# Now it will run different experiments with different amount of WORKERS nodes.

WORKERS=1
# First, we copy the number of workers we want to the workers file
head -1 workers_list.txt > workers.txt

# Then, we start everything. This means:
### Creating the singularity instance in the master node and the workers nodes
### Init spark and HDFS in the master node and the workers nodes
### Upload files to HDFS
./start_all.sh master.txt workers.txt $CORES 

# Now we can run the experiments. It will run different number of files to check times (see script)
./run_np_experiments.sh $CORES $WORKERS

# We stop everything before doing the next experiment
./stop_all.sh master.txt workers.txt

# Uncomment 'exit' if you want to stop with just 1 experiment
#exit

### ADD EXPERIMENTS WITH A DIFFERENT AMOUNT OF WORKERS

WORKERS=2
head -2 workers_list.txt > workers.txt

./start_all.sh master.txt workers.txt $CORES
./run_np_experiments.sh $CORES $WORKERS
./stop_all.sh master.txt workers.txt

WORKERS=3
head -3 workers_list.txt > workers.txt

./start_all.sh master.txt workers.txt $CORES
./run_np_experiments.sh $CORES $WORKERS
./stop_all.sh master.txt workers.txt
