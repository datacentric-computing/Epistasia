# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# IMPORTS
import time, pyspark, timeit, sys, getopt, random
import numpy as np
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import *

# PATHS
#DATAPATH = "/home/hadoop/genomics-ML/epistasis/sparkmdr/data/NuGENE_bychr_repaired/"
DATAPATH = "/home/ggomez/epistasis/data/"

####################
#### ARGUMENTS #####
####################

# Default arguments
n_variants = 20
output_dir = "default" + str(random.randint(0,9999))
n_workers = 16

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hv:w:"
 
# Long options
long_options = ["help", "variants", "workers"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)
     
    # Checking each argument
    for currentArgument, currentValue in arguments:
 
        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-v, --variants [int]: use this option followed by an integer to indicate the number of variants to be processed.")
            print("-w, --workers [int]: indicate the number of partitions of the rdd dataset. It should be a multiple of the number of cores available.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-v", "--variants"):
            n_variants = int(currentValue)
            print(("Number of variants to process set to %s.") % currentValue)


        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-w", "--workers"):
            n_workers = int(currentValue)
            print(("Number of workers available %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Number of variants to process set to all.")
    print(("Output files will be save in %s directory.") % output_dir)
    print("Number of partitions set to default.")


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Table to convert 2 SNPs information to the interaction of both
def map2epi(x):
    if (x==[1,0,0,1,0,0]).all():
        return [1,0,0,0,0,0,0,0,0]
    if (x==[0,1,0,1,0,0]).all():
        return [0,1,0,0,0,0,0,0,0]
    if (x==[0,0,1,1,0,0]).all():
        return [0,0,1,0,0,0,0,0,0]
    if (x==[1,0,0,0,1,0]).all():
        return [0,0,0,1,0,0,0,0,0]
    if (x==[0,1,0,0,1,0]).all():
        return [0,0,0,0,1,0,0,0,0]
    if (x==[0,0,1,0,1,0]).all():
        return [0,0,0,0,0,1,0,0,0]
    if (x==[1,0,0,0,0,1]).all():
        return [0,0,0,0,0,0,1,0,0]
    if (x==[0,1,0,0,0,1]).all():
        return [0,0,0,0,0,0,0,1,0]
    if (x==[0,0,1,0,0,1]).all():
        return [0,0,0,0,0,0,0,0,1]
    else:
        return [0,0,0,0,0,0,0,0,0]

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):

    # Get array of patients and transform to numpy
    patients1 = np.array(x[1][0])
    patients2 = np.array(x[1][1])

    # Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))

    # Concatenate to a one array of 6 x n patients
    patients = np.concatenate((patientsdf1, patientsdf2), axis = 1)

    # Filter imputation data
    #round09vec = np.vectorize(round09)
    patients = imputfilter(patients)

    # Obtain one hot encoding from 6 x n patients to 9 x n patients
    patients = np.apply_along_axis(map2epi, 1, patients)

    return (x[0], patients)

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(x):

    # Get patients
    patients = x[1]

    # Sum only the cases from training set
    cases = patients*npcases*trainset
    sumcases = cases.sum(axis=0)

    # Sum only the controls
    controls = patients*npcontrols*trainset
    sumcontrols = controls.sum(axis=0)

    # Get risk array
    #risk = sumcases/sumcontrols
    #risk[np.isnan(risk)] = 0
    risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)


    # Transform to high risk = 1, low risk = 0
    risk[risk >= ccratio] = 1
    risk[risk < ccratio] = 0

    # Classify training set
    prediction = np.array((patients*risk).sum(axis = 1)).astype(int)

    # Get clasification error
    trainerror = (((npcases.T[0] == prediction)*trainset.T)[0]).sum()/Ntrain

    # Classify test set
    testerror = (((npcases.T[0] == prediction)*testset.T)[0]).sum()/Ntest


    return (x[0], trainerror, testerror)

# Transform to key + values
def keyval(x):
    aux = x.split()
    key = aux[1]
    val = aux[5:]
    return (key,val)

# Function to combine keys sorted
def combine(x):
    aux = sorted((x[0][0],x[1][0]))
    newkey = aux[0] + "_" + aux[1]
    return (newkey, (x[0][1], x[1][1]))


################
##### MAIN #####
################

# Get Spark Context
sc = SparkContext()

# Create Spark Session
spark = SparkSession.builder.appName("example-pyspark-read-and-write").getOrCreate()

# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv(DATAPATH + "input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)
        
# Filter first non relevant row
labels = labels.filter("bin1!='B'")

# Get np array with cases == 1
npcases = np.array(labels.select("bin1").collect()).astype(int)

# Get np array with controls == 1
npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
print("Ration cases/controls: " + str(ccratio[0]))

# Create training and test set
Npatients = 1128
Ntrain = Npatients/5*4
Ntest = Npatients/5
trainset = np.array([np.random.choice([0, 1], size=(1128,), p=[1./5, 4./5])]).T
testset = np.where((trainset==0)|(trainset==1), trainset^1, trainset)

# Read from HDFS
rdd = spark.sparkContext.textFile(DATAPATH + "input/NuGENE_chr22_repaired.gz")
chrvariants = 148791

# Keep only N rows for testing
rdd_reduced = rdd.sample(False, n_variants/chrvariants, 5)
rdd_reduced = rdd_reduced.repartition(n_workers)

# Save as ID_VARIANT : PATIENTS
rdd_kv = rdd_reduced.map(keyval)

# Create new rdd with tuples of every combination
cartesianrdd = rdd_kv.cartesian(rdd_kv)

# Mix the keys of both tuples
newkeyrdd = cartesianrdd.map(lambda x : combine(x))

# Reduce duplicates
snpsrdd = newkeyrdd.reduceByKey(lambda x,y: (x))

# Calculate execution time
starttime = timeit.default_timer()

# Vectorize function
imputfilter = np.vectorize(filter_imputation)

# Transform every SNP-SNP row into the allele combination of SNPs using one hot encoder
epirdd = snpsrdd.map(lambda x: transform_patients(x))

# Obtain the high risk predictor and the classification error
epicount = epirdd.map(lambda x: get_risk_array(x))

# Save data
epicount.saveAsTextFile(DATAPATH + "output/" + output_dir)

print("Total time: ", timeit.default_timer() - starttime)

