# Read workers file
# By deafult the first worker will be set up as the master and a worker
# Pease make sure you have ssh connection to all the workers 
# Please make sure that the all the workers have the spark.sif in a folder named spark in their root user directory or change the path in the code below

# Set Master
MASTER=$(head -n 1 $1)

# Create spark instances
while read -u10 WORKER; do 
	ssh $WORKER mkdir /tmp/spark/log /tmp/spark/work
        ssh $WORKER singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work spark.sif spark
done 10< $1

# Init master
ssh $MASTER singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081

# Init workers
n=1
while read -u10 WORKER; do 
        port=$((8081+$n))
	ssh $WORKER singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n --webui-port $port $MASTER:7078;
        n=$(($n+1))
done 10< $1



