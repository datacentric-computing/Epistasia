# Read workers file
# By deafult the first worker will be set up as the master and a worker

# Set Master
MASTER=$(head -n 1 $1)

# Stop master
ssh $MASTER singularity exec instance://spark spark-daemon.sh stop org.apache.spark.deploy.master.Master 1

# Stop workers
n=1
while read -u10 WORKER; do 
        port=$((8081+$n))
	ssh $WORKER singularity exec instance://spark spark-daemon.sh stop org.apache.spark.deploy.worker.Worker $n
        n=$(($n+1))
done 10< $1

# Create spark instances
while read -u10 WORKER; do 
        ssh $WORKER singularity instance stop spark
done 10< $1





