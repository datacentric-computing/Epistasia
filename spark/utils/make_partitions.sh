FILE=/home/ubuntu/epistasis/data/input/NuGENE_chr22_repaired.gz
SUFFIX=chr22_NuGENE
OUTPATH=/home/ubuntu/epistasis/data/input/split_data
FILE_SIZE=1000

OUTFILE=${FILE%.*}
echo $OUTFILE

mkdir -p $OUTPATH/partitions

gzip -d -k $FILE
split -l $FILESIZE -d $OUTFILE $OUTPATH/partitions/$SUFFIX
gzip $OUTPATH/partitions/*

# Create list of partitions
ls $OUTPATH/partitions/ > $OUTPATH/listoffiles.txt

