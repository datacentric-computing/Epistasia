if [[ $# -eq 0 ]] ; then
    echo 'Please introduce the number of combinations files to process'
    exit 1
fi

C=$1
n=0
while read line
do
    echo "Line No. $n : $line"
    n=$((n+1))
    if [[ "$n" == "$C" ]]
        then
                echo "Number $n, stopping"
                break
        fi
    # Throw job
    sbatch test_cpu_mdr.sh $line

done < combinations.txt

head -n +$C combinations.txt >> done_combinations.txt
tail -n +$(($C+1)) combinations.txt > combinations.txt



