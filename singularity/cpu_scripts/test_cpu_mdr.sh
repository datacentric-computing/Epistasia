#!/bin/bash
#SBATCH --qos=bsc_cs
#SBATCH --time=00:40:00
#SBATCH --error=logs/test_output.stderr
#SBATCH --output=logs/test_output.stdout
#SBATCH -N 1
#SBATCH -c 48

start=`date +%s`

echo "Init instance"
# Init instance and HDFS
./standalone_start.sh

file=$1

echo "Running script"
# Run script
while read line; do
    IFS='-'
    read -ra split <<< "$line" 
    echo ${split[0]} ${split[1]} $file
    ./cpu_run.sh ${split[0]} ${split[1]} $file
done < combinations/$file

echo "Downloading data"
# Download data
singularity exec instance://standalone_mdr hdfs dfs -get /output results/

echo "Closing everything"
# Stop HDFS and instance
./standalone_stop.sh
end=`date +%s`
runtime=$((end-start))
echo $runtime

