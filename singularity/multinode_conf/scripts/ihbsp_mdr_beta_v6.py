# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# Version 6 is build over the snps_combination.py, where we break the NuGENE file into small
# pieces and process every piece against each other. This allows us to not have memory issues
# without having to process it in the script. Should be faster.

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, happybase
import numpy as np
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime

# PATHS
MASTER = "bscdc18"
THSERVERS = ["bscdc18", "bscdc19", "bscdc20"]
NSERVERS = len(THSERVERS)
THPORT = 9091
HDFSPATH = "hdfs://"+MASTER+":9000/"


####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
time = now.strftime("%m%d%y%H%M")

# Default arguments
nPart = 8 
inputfile = "NuGENE_chr22_repaired.gz"

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hsv:o:p:"

# Long options
long_options = ["help", "variants", "output", "partitions"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-v, --variants [int]: use this option followed by an integer to indicate the number of variants to be processed.")
            print("-o, --output [string]: indicate the name of the output file.")
            print("-p, --partitions [int]: indicate the number of partitions of the rdd dataset. It should be a multiple of the number of cores available.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-v", "--variants"):
            variants = currentValue
            fraction = int(variants)/chrvariants
            print(("Number of variants (aprox) to be processed %s.") % currentValue)

        # Set the output directory
        elif currentArgument in ("-o", "--output"):
            output_dir = currentValue
            print(("Output files will be save in %s directory.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-p", "--partitions"):
            nPart = int(currentValue)
            print(("Number of partitions of the rdd set to %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Number of variants to process set to all.")
    print(("Output files will be save in %s directory.") % output_dir)
    print("Number of partitions set to default.")

output_dir = "_d" + str(time)


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Vectorize function
imputfilter = np.vectorize(filter_imputation)


# Table to convert 2 SNPs information to the interaction of both
def map2epi(x):
    if (x==[1,0,0,1,0,0]).all():
        return [1,0,0,0,0,0,0,0,0]
    if (x==[0,1,0,1,0,0]).all():
        return [0,1,0,0,0,0,0,0,0]
    if (x==[0,0,1,1,0,0]).all():
        return [0,0,1,0,0,0,0,0,0]
    if (x==[1,0,0,0,1,0]).all():
        return [0,0,0,1,0,0,0,0,0]
    if (x==[0,1,0,0,1,0]).all():
        return [0,0,0,0,1,0,0,0,0]
    if (x==[0,0,1,0,1,0]).all():
        return [0,0,0,0,0,1,0,0,0]
    if (x==[1,0,0,0,0,1]).all():
        return [0,0,0,0,0,0,1,0,0]
    if (x==[0,1,0,0,0,1]).all():
        return [0,0,0,0,0,0,0,1,0]
    if (x==[0,0,1,0,0,1]).all():
        return [0,0,0,0,0,0,0,0,1]
    else:
        return [0,0,0,0,0,0,0,0,0]


# Apply MDR to every SNP-SNP combination reading from a dict
def apply_mdr_dict(x, rd1, rd2):
    key1 = x[0]
    key2 = x[1]
    row1 = rd1[key1]
    row2 = rd2[key2]

    patients = transform_patients((row1,row2))
    
    #testerror = get_risk_array(patients)

    #return (row1, row2)
    return x[0], len(patients)
    #return x[0], testerror

# Apply MDR to every SNP-SNP combination
def apply_mdr(x):
    key1 = x[0]
    key2 = x[1]
    rows = read_hbase_rows(key1, key2)
    
    #patients = transform_patients(rows)
    
    #testerror = get_risk_array(patients)
    
    return rows
    #return x[0], testerror


# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):
    # Transform to numpy arrays HBASE
    #patients1 = np.frombuffer(x[0][1][b'rsid:patients'], int)
    #patients2 = np.frombuffer(x[1][1][b'rsid:patients'], int)

    #1 - Transform to numpy arrays NO HBASE
    patients1 = np.array(x[0])
    patients2 = np.array(x[1])

    #if len(rows[0] + len(rows[1]) < 1):
        #break
        
    #2 - Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))
    
    #3 - Concatenate to a one array of 6 x n patients
    patients = np.concatenate((patientsdf1, patientsdf2), axis = 1)
        
    #4 - Obtain one hot encoding from 6 x n patients to 9 x n patients
    #patients = np.apply_along_axis(map2epi, 1, patients)

    return patients

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(patients):

    # Error list
    #trainerror = list()
    testerror = list()
    
    for i in range(5):
        
        traindata = trainset[i]
        testdata = testset[i]
        
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)
    
        # Sum only the cases from training set
        cases = patients*npcases*traindata
        sumcases = cases.sum(axis=0)

        # Sum only the controls
        controls = patients*npcontrols*traindata
        sumcontrols = controls.sum(axis=0)

        # Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # Classify training set
        prediction = np.array((patients*risk).sum(axis = 1)).astype(int)

        # Get clasification error
        #trainerror.append((((npcases.T[0] == prediction)*traindata.T)[0]).sum()/Ntrain)

        # Classify test set
        testerror.append((((npcases.T[0] == prediction)*testdata.T)[0]).sum()/Ntest)

    return testerror

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[0] + '-'+ aux[1] +'-'+ aux[2]
    val = imputfilter(aux[5:])
    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)

def create_table_hbase():
    conn = happybase.Connection(random_th_server(), port=THPORT)
    conn.create_table('genomeinfo',{'rsid': dict(),})
    conn.close()

# Save a row to HBase
def row_to_hbase(x):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table("genomeinfo")
    row_bytes = x[1].tobytes()
    table.put(x[0],{b'rsid:patients':row_bytes})
    conn.close()
    return x

# Read 2 rows from HBase
def read_hbase_rows(key1, key2):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table("genomeinfo")
    rows = table.rows([key1, key2])
    conn.close()
    return rows

def random_th_server():
    return(THSERVERS[int(random.random()*NSERVERS)])


################
##### MAIN #####
################

print("Starting main...")
    
# Get Spark Context
#sc = SparkContext()

# Create Spark Session
#spark = SparkSession.builder.appName("example-pyspark-read-and-write").getOrCreate()

conf = SparkConf()
conf.setMaster('spark://bscdc18:7078')
conf.set("spark.executors.cores", "8")
conf.set("spark.num.executors", "17")
conf.set("spark.executor.memory", "16GB")
conf.setAppName('spark-basic')

sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

print("Connected with spartk")

# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv(HDFSPATH + "/input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

print("Patients read")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)
        
# Filter first non relevant row
labels = labels.filter("bin1!='B'")

# Get np array with cases == 1
npcases = np.array(labels.select("bin1").collect()).astype(int)

# Get np array with controls == 1
npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
print("Ration cases/controls: " + str(ccratio[0]))

# Create training and test set
Npatients = 1128
Ntrain = Npatients/5*4
Ntest = Npatients/5

# Create training and test set for a 5-CV 
Npatients = 1128
block_size = Npatients/5

trainset = list()
testset = list()

# Create two list with the train and test indexes
for i in range(5):
    nptrain = np.array([np.ones(Npatients, dtype=int)]).T
    nptrain[int(i*block_size):int(i*block_size + block_size)] = 0
    nptest = np.where((nptrain==0)|(nptrain==1), nptrain^1, nptrain)
    
    trainset.append(nptrain)
    testset.append(nptest)

# Read files to be processed
files = sc.textFile(HDFSPATH + "/input/listoffiles.txt").collect()

# Create HBASE table
print("Connecting to HBASE")
conn = happybase.Connection(MASTER, port=9091)
tables = conn.tables()
print("Tables in HBASE: ", tables, ". Connection done.")
conn.close()
if b'genomeinfo' not in tables:
    print("Creating new table..")
    create_table_hbase()
    print("..table created.")


starttime0 = timeit.default_timer()

nf = 1
n1 = 0
n2 = 0


print("START COMPUTING FILES.")

# Compute all the files against all the files
for f1 in files:
    
    print("Loading file 1: " + f1)
    starttimef1 = timeit.default_timer()

    # Read file 1
    rdd1 = spark.sparkContext.textFile(HDFSPATH +"/input/partitions/" + f1)

    # Convert into key-val rdd
    rdd1 = rdd1.map(get_keyval)

    # Save data to HBASE
    #rdd1 = rdd1.map(row_to_hbase)
    rdd1dict = rdd1.collectAsMap()

    # Keep only the keys information
    rdd1 = rdd1.keys()

    rdd1 = sc.parallelize(rdd1.collect(), nPart)

    print("File 1 loaded in : ", timeit.default_timer()-starttimef1)
    print("RDD1 partitions:", rdd1.getNumPartitions())
    print("RDD1 variants:", rdd1.count())

    # Compute against all the other files
    for f2 in files:
        rdd2 = spark.sparkContext.textFile(HDFSPATH +"/input/partitions/" + f2)

        # Convert into key-val rdd
        rdd2 = rdd2.map(get_keyval)

        # Save data to HBASE
        #rdd2 = rdd2.map(row_to_hbase)
        rdd2dict = rdd2.collectAsMap()

        # Keep only the keys information
        rdd2 = rdd2.keys()
        
        rdd2 = sc.parallelize(rdd2.collect(), nPart)
        #print("RDD2 partitions:", rdd2.getNumPartitions())
        #print("RDD2 variants:", rdd2.count())

        # Calculate execution time
        starttimerf2 = timeit.default_timer()

        # Create new rdd with tuples of every combination
        cartesiankeys = rdd1.cartesian(rdd2)

        #cartesiankeys.count()

        # Compute MDR
        #mdrerror = cartesiankeys.map(apply_mdr)
        #mdrerror.count()
        mdrerror = cartesiankeys.map(lambda x: apply_mdr_dict(x, rdd1dict, rdd2dict))
        print("MDR APPLIED TO: ", mdrerror.count())
        print(mdrerror.take(2))


        #print("Total number of combinations: ", cartesiankeys.count())
        #print("Combined f1 & f2 in: " , timeit.default_timer()-starttimerf2)
        
        n2+=1
        if n2==nf:
            n2 = 0
            break


    print("Combined f1 in: " , timeit.default_timer()-starttimef1)
    n1+=1
    if n1==nf:
        break


print("Total time: ", timeit.default_timer()-starttime0)
