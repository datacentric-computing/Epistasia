# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# IMPORTS
import time, pyspark, timeit, sys, getopt, random
import numpy as np
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.conf import SparkConf

################
##### MAIN #####
################

# Get Spark Context

# Get Spark Context
sc = SparkContext()

# Create Spark Session
spark = SparkSession.builder.appName("mdr-hasp").getOrCreate()

print(sc.getConf().getAll())

conf = spark.sparkContext._conf.setAll([('spark.executor.memory', '40g'), ('spark.app.name', 'Spark Updated Conf'), ('spark.executor.cores', '4'), ('spark.cores.max', '4'), ('spark.driver.memory','40g')])

sc.stop()

spark = SparkSession.builder.config(conf=conf).appName("mdr-hasp").getOrCreate()

print(sc.getConf().getAll())


