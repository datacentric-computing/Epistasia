# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, happybase
import numpy as np
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime

# PATHS
DATAPATH = "/home/ggomezsbs/epistasis/data/NuGENE_bychr_repaired/"
#HDFSPATH = "hdfs://localhost:9000/"
MASTER = "bscdc18"
HDFSPATH = "hdfs://"+MASTER+":9000/"


####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
time = now.strftime("%m%d%y%H%M")

# Default arguments
n_slices = 4 #not used right now
ch = "ch22_"
chrvariants = 148791
variants = 500 
fraction = variants/chrvariants
inputfile = "NuGENE_chr22_repaired.gz"

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hsv:o:p:"

# Long options
long_options = ["help", "variants", "output", "partitions"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-v, --variants [int]: use this option followed by an integer to indicate the number of variants to be processed.")
            print("-o, --output [string]: indicate the name of the output file.")
            print("-p, --partitions [int]: indicate the number of partitions of the rdd dataset. It should be a multiple of the number of cores available.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-v", "--variants"):
            variants = currentValue
            fraction = int(variants)/chrvariants
            print(("Number of variants (aprox) to be processed %s.") % currentValue)

        # Set the output directory
        elif currentArgument in ("-o", "--output"):
            output_dir = currentValue
            print(("Output files will be save in %s directory.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-p", "--partitions"):
            n_slices = int(currentValue)
            print(("Number of partitions of the rdd set to %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Number of variants to process set to all.")
    print(("Output files will be save in %s directory.") % output_dir)
    print("Number of partitions set to default.")

output_dir = "results_v" + str(variants) + "_d" + str(time)


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Vectorize function
imputfilter = np.vectorize(filter_imputation)


# Table to convert 2 SNPs information to the interaction of both
def map2epi(x):
    if (x==[1,0,0,1,0,0]).all():
        return [1,0,0,0,0,0,0,0,0]
    if (x==[0,1,0,1,0,0]).all():
        return [0,1,0,0,0,0,0,0,0]
    if (x==[0,0,1,1,0,0]).all():
        return [0,0,1,0,0,0,0,0,0]
    if (x==[1,0,0,0,1,0]).all():
        return [0,0,0,1,0,0,0,0,0]
    if (x==[0,1,0,0,1,0]).all():
        return [0,0,0,0,1,0,0,0,0]
    if (x==[0,0,1,0,1,0]).all():
        return [0,0,0,0,0,1,0,0,0]
    if (x==[1,0,0,0,0,1]).all():
        return [0,0,0,0,0,0,1,0,0]
    if (x==[0,1,0,0,0,1]).all():
        return [0,0,0,0,0,0,0,1,0]
    if (x==[0,0,1,0,0,1]).all():
        return [0,0,0,0,0,0,0,0,1]
    else:
        return [0,0,0,0,0,0,0,0,0]

# Apply MDR to every SNP-SNP combination
def apply_mdr(x):
    aux = x[0].split("_")
    key1 = aux[0]
    key2 = aux[1]
    rows = read_hbase_rows(key1, key2)
    
    patients = transform_patients(rows)
    
    testerror = get_risk_array(patients)
    
    return x[0], testerror


# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):
    # Transform to numpy arrays
    patients1 = np.frombuffer(x[0][1][b'rsid:patients'], int)
    patients2 = np.frombuffer(x[1][1][b'rsid:patients'], int)
    
    #if len(rows[0] + len(rows[1]) < 1):
        #break
        
    # Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))
    
    # Concatenate to a one array of 6 x n patients
    patients = np.concatenate((patientsdf1, patientsdf2), axis = 1)
        
    # Obtain one hot encoding from 6 x n patients to 9 x n patients
    patients = np.apply_along_axis(map2epi, 1, patients)

    return patients

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(patients):

    # Error list
    #trainerror = list()
    testerror = list()
    
    for i in range(5):
        
        traindata = trainset[i]
        testdata = testset[i]
        
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)
    
        # Sum only the cases from training set
        cases = patients*npcases*traindata
        sumcases = cases.sum(axis=0)

        # Sum only the controls
        controls = patients*npcontrols*traindata
        sumcontrols = controls.sum(axis=0)

        # Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # Classify training set
        prediction = np.array((patients*risk).sum(axis = 1)).astype(int)

        # Get clasification error
        #trainerror.append((((npcases.T[0] == prediction)*traindata.T)[0]).sum()/Ntrain)

        # Classify test set
        testerror.append((((npcases.T[0] == prediction)*testdata.T)[0]).sum()/Ntest)

    return testerror

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[0] + '-'+ aux[1] +'-'+ aux[2]
    val = imputfilter(aux[5:])
    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)

def create_table_hbase():
    conn = happybase.Connection(MASTER)
    conn.create_table('genomeinfo',{'rsid': dict(),})
    conn.close()

# Save a row to HBase
def row_to_hbase(x):
    conn = happybase.Connection(MASTER)
    table = conn.table("genomeinfo")
    row_bytes = x[1].tobytes()
    table.put(x[0],{b'rsid:patients':row_bytes})
    conn.close()
    return x

# Read 2 rows from HBase
def read_hbase_rows(key1, key2):
    conn = happybase.Connection('localhost')
    table = conn.table("genomeinfo")
    rows = table.rows([key1, key2])
    conn.close()
    return rows

################
##### MAIN #####
################

print("Starting main...")
    
# Get Spark Context
sc = SparkContext()

# Create Spark Session
spark = SparkSession.builder.appName("example-pyspark-read-and-write").getOrCreate()

print("Connected with spartk")

# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv(HDFSPATH + "/input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

print("Patients read")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)
        
# Filter first non relevant row
labels = labels.filter("bin1!='B'")

# Get np array with cases == 1
npcases = np.array(labels.select("bin1").collect()).astype(int)

# Get np array with controls == 1
npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
print("Ration cases/controls: " + str(ccratio[0]))

# Create training and test set
Npatients = 1128
Ntrain = Npatients/5*4
Ntest = Npatients/5

# Create training and test set for a 5-CV 
Npatients = 1128
block_size = Npatients/5

trainset = list()
testset = list()

# Create two list with the train and test indexes
for i in range(5):
    nptrain = np.array([np.ones(Npatients, dtype=int)]).T
    nptrain[int(i*block_size):int(i*block_size + block_size)] = 0
    nptest = np.where((nptrain==0)|(nptrain==1), nptrain^1, nptrain)
    
    trainset.append(nptrain)
    testset.append(nptest)

# Read from HDFS
rdd = spark.sparkContext.textFile(HDFSPATH +"/input/" + inputfile)

# Create HBASE table if needed
print("Connecting to HBASE")
conn = happybase.Connection(MASTER)
tables = conn.tables()
print("Tables in HBASE: ", tables)
ntables = len(tables)
conn.close()
if ntables < 1:
    print("Creating new table..")
    create_table_hbase()
    print("..table created.")
print("Connection done")

# Reduce the rdd to compute only a fraction of the data
rdd_reduced = rdd.sample(False, fraction, 5)

# Get number of variants being processed
starttime = timeit.default_timer()
real_variants = rdd_reduced.count()
print("Final number of variants to be processed: " + str(real_variants) + " counted in " + str(timeit.default_timer() - starttime))

# Convert into key-val rdd
rdd_kv = rdd_reduced.map(get_keyval)

# Save all the rows to hbase
rdd_kv = rdd_kv.map(row_to_hbase)

# Keep only the keys information
rdd_keys = rdd_kv.keys()

# Create new rdd with tuples of every combination
cartesiankeys = rdd_keys.cartesian(rdd_keys)

# Combine into one single key
combinedkeys = cartesiankeys.map(combine)

# Reduce duplicates
reducekeys = combinedkeys.reduceByKey(lambda x,y: (x))

# Remove Nones
y = sc.parallelize([('NONE', 0)])
clean_keys = reducekeys.subtractByKey(y)

# Calculate execution time
starttime = timeit.default_timer()

# Apply MDR to every key combination
mdrerror = clean_keys.map(lambda x: apply_mdr(x))

# Save data
mdrerror.saveAsTextFile(HDFSPATH + "output/" + output_dir)

print("Total time: ", timeit.default_timer() - starttime)

