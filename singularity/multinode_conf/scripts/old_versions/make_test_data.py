import gzip, sys

DATAPATH = "/home/ggomez/epistasis/singularity/data/input/"


n = int(sys.argv[1])

with gzip.open(DATAPATH + "NuGENE_chr22_repaired.gz",'rb') as myfile:
    head = [next(myfile) for x in range(n)]

f_out = gzip.open(DATAPATH + "NuGENE_chr22_"+str(n)+"variants.gz", 'wb')
f_out.writelines(head)
f_out.close()
