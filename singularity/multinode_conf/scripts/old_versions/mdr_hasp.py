# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# IMPORTS
import time, pyspark, timeit, sys, getopt, random
import numpy as np
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime


# DEFAULT PATHS
HDFSPATH = "hdfs://bscdc18:9000/"
#HDFSPATH = "hdfs://localhost:9000/"

####################
#### ARGUMENTS #####
####################

# Save results by date&time
now = datetime.now()
time = now.strftime("%m%d%y%H%M")


# Default arguments
inputfile = "NuGENE_chr22_repaired.gz" # around 148.791 variants
chrvariants = 148791
n_slices = 48 # number of cores of bscdc18
fraction = 0.0005 # computing around 100 with this fraction.

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hf:v:r:m:n:p:"
 
# Long options
long_options = ["help", "file", "machine","nmachine", "partitions"]


try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)
     
    # Checking each argument
    for currentArgument, currentValue in arguments:
 
        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-f, --inputfile [string]: data file to be processed.")
            print("-v, --variants [float]: fraction of variants to be processed. Default is 0.0005.")
            print("-r, --rows [int]: number of rows/variants to be processed. Overwrites the value of variants.")
            print("-m, --machine [string]: root path to hdfs.")
            print("-n, --nmachine [int]: number of the bscdc machine where the hdfs is set up")
            print("-p, --partitions [int]: indicate the number of partitions of the rdd dataset. It should be a multiple of the number of cores available.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-f", "--file"):
            inputfile = currentValue
            print(("File to be processed %s.") % currentValue)

        # Set the number of variants to be processed
        elif currentArgument in ("-v", "--variants"):
            fraction = float(currentValue)
            variants = chrvariants*fraction
            print(("Number of variants to be processed: %s.") % variants)

        # Set the number of variants to be processed
        elif currentArgument in ("-r", "--rows"):
            variants = float(currentValue)
            fraction = variants/chrvariants
            print(("Number of variants to be processed: %s.") % variants)

        # Set the hdfs host
        elif currentArgument in ("-m", "--machine"):
            HDFSPATH = "hdfs://" + currentValue
            print(("Using %s as the HDFS host.") % currentValue)

        # Set the bscdc machine
        elif currentArgument in ("-n", "--nmachine"):
            HDFSPATH = "hdfs://bscdc" + currentValue + ":9000/"
            print(("Using bscdc  %s as the HDFS host.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-p", "--partitions"):
            n_slices = int(currentValue)
            print(("Number of partitions of the rdd set to %s.") % currentValue)


# Default values
except getopt.error as err:
    print(("Output files will be save in %s directory.") % output_dir)
    print("Number of partitions set to default.")
    print("Using machine number 18.")


# Output directory
output_dir = "results_v" + str(variants) + "_p" + str(n_slices) + "_d" + time

#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Table to convert 2 SNPs information to the interaction of both
def map2epi(x):
    if (x==[1,0,0,1,0,0]).all():
        return [1,0,0,0,0,0,0,0,0]
    if (x==[0,1,0,1,0,0]).all():
        return [0,1,0,0,0,0,0,0,0]
    if (x==[0,0,1,1,0,0]).all():
        return [0,0,1,0,0,0,0,0,0]
    if (x==[1,0,0,0,1,0]).all():
        return [0,0,0,1,0,0,0,0,0]
    if (x==[0,1,0,0,1,0]).all():
        return [0,0,0,0,1,0,0,0,0]
    if (x==[0,0,1,0,1,0]).all():
        return [0,0,0,0,0,1,0,0,0]
    if (x==[1,0,0,0,0,1]).all():
        return [0,0,0,0,0,0,1,0,0]
    if (x==[0,1,0,0,0,1]).all():
        return [0,0,0,0,0,0,0,1,0]
    if (x==[0,0,1,0,0,1]).all():
        return [0,0,0,0,0,0,0,0,1]
    else:
        return [0,0,0,0,0,0,0,0,0]

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):

    # Get array of patients and transform to numpy
    patients1 = np.array(x[1][0])
    patients2 = np.array(x[1][1])

    # Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))

    # Concatenate to a one array of 6 x n patients
    patients = np.concatenate((patientsdf1, patientsdf2), axis = 1)

    # Filter imputation data
    #round09vec = np.vectorize(round09)
    patients = imputfilter(patients)

    # Obtain one hot encoding from 6 x n patients to 9 x n patients
    patients = np.apply_along_axis(map2epi, 1, patients)

    return (x[0], patients)

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(x):
    
    # Get patients
    patients = x[1]
    
    # Error list
    trainerror = list()
    testerror = list()
    
    for i in range(5):
        
        traindata = trainset[i]
        testdata = testset[i]
        
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)
    
        # Sum only the cases from training set
        cases = patients*npcases*traindata
        sumcases = cases.sum(axis=0)

        # Sum only the controls
        controls = patients*npcontrols*traindata
        sumcontrols = controls.sum(axis=0)

        # Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # Classify training set
        prediction = np.array((patients*risk).sum(axis = 1)).astype(int)

        # Get clasification error
        arr_trainerror = (npcases.T[0] == prediction)*traindata.T[0]
        trainerror.append(arr_trainerror.sum()/Ntrain)

        # Classify test set
        arr_testerror = (npcases.T[0] == prediction)*testdata.T[0]
        testerror.append(arr_testerror.sum()/Ntest)

    return (x[0], trainerror, testerror)


# Transform to key + values
def keyval(x):
    aux = x.split()
    key = ch + aux[1]
    val = aux[5:]
    return (key,val)

# Function to combine keys sorted
def combine(x):
    aux = sorted((x[0][0],x[1][0]))
    if aux[0]!=aux[1]:
        newkey = aux[0] + "_" + aux[1]
        return (newkey, (x[0][1], x[1][1]))
    else:
        return (None, None)

################
##### MAIN #####
################

# Get Spark Context
#sc = SparkContext()

# Create Spark Session
spark = SparkSession.builder.appName("mdr-hasp").getOrCreate()

# Update configuration
conf = spark.sparkContext._conf.setAll([('spark.executor.memory', '512g'), ('spark.driver.memory','512g'), ('spark.driver.maxResultSize','512g')])

# Reboot spark
spark.stop()
spark = SparkSession.builder.config(conf=conf).appName("mdr-hasp").getOrCreate()

# Get Spark Context()
sc = spark.sparkContext

print("Spark configuration: ")
configurations = spark.sparkContext.getConf().getAll()
for item in configurations: print(item)



# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv(HDFSPATH + "input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)
        
# Filter first non relevant row
labels = labels.filter("bin1!='B'")

# Get np array with cases == 1
npcases = np.array(labels.select("bin1").collect()).astype(int)

# Get np array with controls == 1
npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
print("Ration cases/controls: " + str(ccratio[0]))

# Create training and test set
Npatients = 1128
block_size = Npatients/5

trainset = list()
testset = list()

# Create two list with the train and test indexes
for i in range(5):
    nptrain = np.array([np.ones(Npatients, dtype=int)]).T
    nptrain[int(i*block_size):int(i*block_size + block_size)] = 0
    nptest = np.where((nptrain==0)|(nptrain==1), nptrain^1, nptrain)
    
    trainset.append(nptrain)
    testset.append(nptest)

# Read from HDFS
ch = "ch22_"
rdd = spark.sparkContext.textFile(HDFSPATH +"/input/" + inputfile)

# Divide the rdd into n_slices partitions
rdd_sliced = rdd.repartition(n_slices)

# Reduce the rdd to compute only a fraction of the data
rdd_reduced = rdd_sliced.sample(False, fraction, 5)

# Save as ID_VARIANT : PATIENTS
rdd_kv = rdd_reduced.map(keyval)

# Create new rdd with tuples of every combination
cartesianrdd = rdd_kv.cartesian(rdd_kv)

# Mix the keys of both tuples
newkeyrdd = cartesianrdd.map(lambda x : combine(x))

# Remove Nones
y = sc.parallelize([(None,None)])
newkeyrdd = newkeyrdd.subtractByKey(y)

# Reduce duplicates
snpsrdd = newkeyrdd.reduceByKey(lambda x,y: (x))

# Calculate execution time
starttime = timeit.default_timer()

# Vectorize function
imputfilter = np.vectorize(filter_imputation)

# Transform every SNP-SNP row into the allele combination of SNPs using one hot encoder
epirdd = snpsrdd.map(lambda x: transform_patients(x))

# Obtain the high risk predictor and the classification error
epicount = epirdd.map(lambda x: get_risk_array(x))

# Save data
epicount.saveAsTextFile(HDFSPATH + "output/" + output_dir)

print("Total time: ", timeit.default_timer() - starttime)

