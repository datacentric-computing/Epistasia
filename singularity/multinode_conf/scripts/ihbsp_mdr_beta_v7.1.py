# Apply Multifactor Dimensionality Reduction using Spark to a genomic dataset of a binary treat.
# By Gonzalo Gomez-Sanchez. 

# Version 7 is build over version 6, processing small files. We change the operations to apply
# all methods regarding the risk calculation as matrix multiplication and avoid using dicts.
# It has shown to be way faster working locally.
# For version 7.1 we compute a sample vs sample in every worker

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, happybase, gzip
import numpy as np
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from datetime import datetime
from hdfs import InsecureClient

# PATHS
MASTER = "bscdc18"
THSERVERS = ["bscdc18", "bscdc19", "bscdc20"]
NSERVERS = len(THSERVERS)
THPORT = 9091
HDFSPATH = "hdfs://"+MASTER+":9000/"


####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
time = now.strftime("%m%d%y%H%M")

# Default arguments
nPart = 8 
inputfile = "NuGENE_chr22_repaired.gz"

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hsv:o:p:"

# Long options
long_options = ["help", "variants", "output", "partitions"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-v, --variants [int]: use this option followed by an integer to indicate the number of variants to be processed.")
            print("-o, --output [string]: indicate the name of the output file.")
            print("-p, --partitions [int]: indicate the number of partitions of the rdd dataset. It should be a multiple of the number of cores available.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-v", "--variants"):
            variants = currentValue
            fraction = int(variants)/chrvariants
            print(("Number of variants (aprox) to be processed %s.") % currentValue)

        # Set the output directory
        elif currentArgument in ("-o", "--output"):
            output_dir = currentValue
            print(("Output files will be save in %s directory.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-p", "--partitions"):
            nPart = int(currentValue)
            print(("Number of partitions of the rdd set to %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Number of variants to process set to all.")
    print(("Output files will be save in %s directory.") % output_dir)
    print("Number of partitions set to default.")

output_dir = "_d" + str(time)


#####################
##### FUNCTIONS #####
#####################

## HBASE

# Random acces to thrift
def random_th_server():
    return(THSERVERS[int(random.random()*NSERVERS)])

# Create HBase table
def create_table_hbase(table_name=b'genomeinfo'):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    tables = conn.tables()
    if table_name not in tables:
        conn.create_table(table_name,{'rsid': dict(),})
    conn.close()

# Save a row to HBase
def row_to_hbase(key, row, table_name= "genomeinfo"):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table(table_name)
    data = {b'rsid:row':row}
    table.put(key,data)
    conn.close()

# Read 2 rows from HBase
def read_hbase_rows(key1, key2, table_name="genomeinfo"):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table(table_name)
    rows = table.rows([key1, key2])
    conn.close()
    return rows

def read_hbase_row(key1, table_name="genomeinfo"):
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table(table_name)
    row = table.row(key1)
    conn.close()
    return row



# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Convert sample from hbase to dict
def hbasesample_2_dict(hbrow):

    sample = str(hbrow).replace("rs", "chr").replace("'","").replace(",","").replace("}","").replace(")","").split("chr")
    
    filekv = dict()
    slen = len(sample)
    n = 0
    for snp in sample:
        snp = snp.split()
        key = "chr" +  snp[0]
        if n == slen:
            data = [filter_imputation(x) for x in snp[4:-1]]
        else:
            data = [filter_imputation(x) for x in snp[4:]]
        
        filekv[key] = data
        n+=1

    return filekv

# Apply MDR to 2 files
def apply_mdr(files):
     
    fn1 = files[0]
    fn2 = files[1]

    client_hdfs = InsecureClient('http://' +MASTER+  ':9870')

    t = 100000
    n = 0
    m = 0

    filekv1 = dict()
    with client_hdfs.read('/input/small_partitions/' + fn1) as reader:
        # Read lines. TRY TO IMPROVE WITH PANDAS
        with gzip.open(reader, 'rt') as f:
            for line in f:
                n += 1
                if n ==t:
                    break
                aux = line.split()
                filekv1[aux[1]] = [float(x) for x in aux[5:]]

    filekv2 = dict()
    with client_hdfs.read('/input/small_partitions/' + fn2) as reader:
        # Read lines. TRY TO IMPROVE WITH PANDAS
        with gzip.open(reader, 'rt') as f:
            for line in f:
                m += 1
                if m ==t:
                    break
                aux = line.split()
                filekv2[aux[1]] = [float(x) for x in aux[5:]]

    # Get all the SNPs of file and save them to the table
    conn = happybase.Connection(random_th_server(), port=THPORT)
    table = conn.table(b'error_table')
    batch = table.batch()

    for k1, v1 in filekv1.items():
        for k2, v2 in filekv2.items():

            epik = k1 + '-' + k2
            epiv = mdr(v1,v2)
            epivytes = [str(x) for x in epiv]
            epivytes = "".join(epivytes)
            batch.put(epik, {b'rsid:patients':epivytes})

    # Close Hbase connections
    batch.send()
    conn.close()

    return 0

# Apply MDR to 2 SNPS
def mdr(snp1, snp2):

    patients = transform_patients(snp1, snp2)
    testerror = classify_patients(patients)

    return testerror

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(snp1, snp2):

    #1 - Transform to numpy arrays NO HBASE
    patients1 = snp1
    patients2 = snp2

    #2 - Reshape as 3 x n patients
    patientsdf1 = np.reshape(patients1, (int(len(patients1)/3),3))
    patientsdf2 = np.reshape(patients2, (int(len(patients2)/3),3))
    
    #3 - Transform to integer
    pt1 = np.matmul(patientsdf1,np.transpose([1,2,3]))*3
    pt2 = np.matmul(patientsdf2,np.transpose([0,1,2]))
    ptcode = pt1-pt2

    return ptcode

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def classify_patients(patients):

    # Error list
    #trainerror = list()
    testerror = list()
    
    for i in range(5):
        
        # 1 - Get the sets for the iteration
        traindata = trainset[i]
        testdata = testset[i]
        Ntrain = np.sum(traindata)
        Ntest = np.sum(testdata)

        # 2 - Sum only the cases from training set
        cases = patients*npcases.T*traindata.T
        sumcases = count_occurrences(cases)

        # 2 - Sum only the controls
        controls = patients*npcontrols.T*traindata.T
        sumcontrols = count_occurrences(controls)

        # 3 - Get risk array
        #risk = sumcases/sumcontrols
        risk = np.divide(sumcases, sumcontrols, out=np.zeros(sumcases.shape, dtype=float), where=sumcontrols!=0)
        #risk[np.isnan(risk)] = 0

        # 4 - Transform to high risk = 1, low risk = 0
        risk[risk >= ccratio] = 1
        risk[risk < ccratio] = 0

        # 5 - Classify training set
        prediction = apply_risk(patients, risk)

        # Get clasification error
        #trainerror.append((((npcases.T[0] == prediction)*traindata.T)[0]).sum()/Ntrain)

        # 6 - Get clasification error
        cv_testerror = (prediction + npcases.T[0])%2
        cv_testerror = (1-cv_testerror)*testdata.T
        error = int(cv_testerror.sum()/Ntest*100)
        testerror.append(error)

    return testerror

# Transform the counts to an array
def count_occurrences(data):
    
    unique, counts = np.unique(data, return_counts=True)
    dtcounts = dict(zip(unique, counts))

    aux = list()
    for i in range(10):
        if i in dtcounts:
            aux.append(dtcounts[i])
        else:
            aux.append(0)
            
    return np.array(aux)

# Apply risk vector to classify the patients
def apply_risk(patients, risk):
    
    prediction = np.zeros(len(patients))
    casevalues = np.where(risk == 1)
    
    for n in casevalues[0][1:]:
        prediction[patients==n] = 1
        
    return prediction.astype(int)

# Vectorize function
imputfilter = np.vectorize(filter_imputation)

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[0] + '-'+ aux[1] +'-'+ aux[2]
    val = imputfilter(aux[5:])
    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)

# Convert tuple to dict
def tuple2dict(data):

    thedict = dict()
    for x in data:
        thedict[x[0]] = x[1]

    return thedict


################
##### MAIN #####
################

print("Starting main...")
    
# Get Spark Context
#sc = SparkContext()

# Create Spark Session
#spark = SparkSession.builder.appName("example-pyspark-read-and-write").getOrCreate()

conf = SparkConf()
conf.setMaster('spark://bscdc18:7078')
conf.set("spark.executors.cores", "8")
conf.set("spark.num.executors", "17")
conf.set("spark.executor.memory", "16GB")
conf.setAppName('spark-basic')

sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

print("Connected with spark")

# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv(HDFSPATH + "/input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

print("Patients read")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)
        
# Filter first non relevant row
labels = labels.filter("bin1!='B'")

# Get np array with cases == 1
npcases = np.array(labels.select("bin1").collect()).astype(int)

# Get np array with controls == 1
npcontrols = np.where((npcases==0)|(npcases==1), npcases^1, npcases)

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = npcases.sum(axis=0)/npcontrols.sum(axis=0)
print("Ration cases/controls: " + str(ccratio[0]))

# Create training and test set
Npatients = 1128
Ntrain = Npatients/5*4
Ntest = Npatients/5

# Create training and test set for a 5-CV 
Npatients = 1128
block_size = Npatients/5

trainset = list()
testset = list()

# Create two list with the train and test indexes
for i in range(5):
    nptrain = np.array([np.ones(Npatients, dtype=int)]).T
    nptrain[int(i*block_size):int(i*block_size + block_size)] = 0
    nptest = np.where((nptrain==0)|(nptrain==1), nptrain^1, nptrain)
    
    trainset.append(nptrain)
    testset.append(nptest)

starttime0 = timeit.default_timer()

# Create HBASE error table
create_table_hbase(b'error_table')

# Number of files to be processed
nf = 100

# Read files to be processed
rddfiles = sc.textFile(HDFSPATH + "/input/small_listoffiles.txt")
rddfiles_sub = sc.parallelize(rddfiles.take(nf))

# Make cartesian product of files
car_files = rddfiles_sub.cartesian(rddfiles_sub)

# Apply Multifactor Dimensionality Reduction method
mdrerror = car_files.map(apply_mdr)
aux = mdrerror.collect()

print(aux[0])

print("PROCESSED: ", len(aux))

print("Total time: ", timeit.default_timer()-starttime0)
