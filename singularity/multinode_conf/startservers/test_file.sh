# Set Master
MASTER="bscdc18"
DATA=~/epistasis/data/input/split_data/*
HEADER=~/epistasis/data/input/phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample

ssh $MASTER singularity exec instance://ihbsp hdfs dfs -mkdir /input
ssh $MASTER singularity exec instance://ihbsp hdfs dfs -mkdir /output

ssh $MASTER singularity exec instance://ihbsp hdfs dfs -put $DATA /input
ssh $MASTER singularity exec instance://ihbsp hdfs dfs -put $HEADER /input

ssh $MASTER singularity exec instance://ihbsp python epistasis/singularity/scripts/snps_combination.py
