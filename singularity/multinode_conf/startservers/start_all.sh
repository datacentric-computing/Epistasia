# Start HDFS and SPARK
./base_start.sh workers.txt

# Start Zookeeper
./zookeeper_start.sh

# Start HBase
./hbase_start.sh

# Start Thrift Server
./thrift_start.sh


