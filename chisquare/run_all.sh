CORES=8
WORKERS=3
head -3 workers_list.txt > workers.txt

./start_all.sh master.txt workers.txt $CORES

singularity exec instance://ihbsp pip install scipy

./run_np_experiments.sh $CORES $WORKERS
./stop_all.sh master.txt workers.txt
