# STOP INSTANCES
# Stop Master
while read -u10 WORKER; do
	echo "Stopping instance in node" $WORKER
        ssh $WORKER singularity instance stop ihbsp
done 10< $1

# Stop Workers
while read -u10 WORKER; do
	echo "Stopping instance in node" $WORKER
        ssh $WORKER singularity instance stop ihbsp
done 10< $2

