# Read workers file
# By default the first worker will be set up as the master and a worker

# Set Master
MASTER=$(head -n 1 $1)
WORKERS=$2
CORES=$3

## CREATE SINGULARITY INSTANCES
# Master
ssh $MASTER singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work ibsc_hbsp_mdr.sif ihbsp

# Workers
while read -u10 WK; do
	echo "Starting instance in node " + WORKER
        ssh $WK rm -r /tmp/*
	ssh $WK mkdir -p /tmp/hdfs/namenode
        ssh $WK mkdir -p /tmp/hdfs/datanode
        ssh $WK mkdir -p /tmp/spark/logs /tmp/spark/work
        ssh $WK singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work ibsc_hbsp_mdr.sif ihbsp
done 10< $WORKERS

# INIT ZOOKEEPER
./zookeeper_start.sh master.txt workers.txt

## INIT SPARK & HDFS
# Init master
ssh $MASTER singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081
ssh $MASTER echo 'Y' | singularity exec instance://ihbsp hdfs namenode -format

ssh $MASTER singularity exec instance://ihbsp hdfs --daemon start namenode
ssh $MASTER singularity exec instance://ihbsp yarn --daemon start resourcemanager

# Init workers
n=1
while read -u10 WK; do
        port=$((8081+$n))
        ssh $WK singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n --webui-port $port $MASTER:7078 --cores $CORES;
        ssh $WK singularity exec instance://ihbsp hdfs --daemon start datanode
        ssh $WK singularity exec instance://ihbsp yarn --daemon start nodemanager
	n=$(($n+1))
done 10< $WORKERS

# Upload files to HDFS
singularity exec instance://ihbsp hdfs dfs -mkdir /input
singularity exec instance://ihbsp hdfs dfs -mkdir /output
singularity exec instance://ihbsp hdfs dfs -put ~/Epistasia/riskv/sdata/input/partitions/* /input
