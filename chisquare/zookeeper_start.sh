# Set Master
MASTER=$(head -n 1 $1)
WORKERS=$2

#MASTER
n=1
ssh $MASTER mkdir -p /tmp/zookeeper
echo $n > /tmp/zookeeper/myid1
scp /tmp/zookeeper/myid1 $MASTER:/tmp/zookeeper/myid
ssh $MASTER singularity exec instance://ihbsp zkServer.sh start

#WORKERS
n=2
while read -u10 WK; do
	ssh $WK mkdir -p /tmp/zookeeper
	echo $n > /tmp/zookeeper/myid1
	scp /tmp/zookeeper/myid1 $WK:/tmp/zookeeper/myid
	ssh $WK singularity exec instance://ihbsp zkServer.sh start
        n=$(($n+1))
done 10< $WORKERS

# View Zookeeper status
ssh bscdc18 singularity exec instance://ihbsp zkServer.sh status
ssh bscdc19 singularity exec instance://ihbsp zkServer.sh status
ssh bscdc20 singularity exec instance://ihbsp zkServer.sh status
