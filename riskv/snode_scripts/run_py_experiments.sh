singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 1
rm -r sdata/output/*

singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 3
rm -r sdata/output/*

singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 5
rm -r sdata/output/*

singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 10
rm -r sdata/output/*

singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 25
rm -r sdata/output/*

singularity run ../singularity/standalone_mdr.sif python sparkmdr_x86.py -f 50
rm -r sdata/output/*
