# Read workers file
# By default the first worker will be set up as the master and a worker

# Set Master
MASTER=$(head -n 1 $1)
WORKERS=$2
CORES=$3

## CREATE SINGULARITY INSTANCES
# Master
ssh $MASTER singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work ibsc_hbsp_mdr.sif ihbsp

# Workers
while read -u10 WK; do
	echo "Starting instance in node " + WORKER
        ssh $WK singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work ibsc_hbsp_mdr.sif ihbsp
done 10< $WORKERS


## INIT SPARK
# Init master
ssh $MASTER singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081

# Init workers
n=1
while read -u10 WK; do
        port=$((8081+$n))
        ssh $WK singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n --webui-port $port $MASTER:7078 --cores $CORES;
        n=$(($n+1))
done 10< $WORKERS

