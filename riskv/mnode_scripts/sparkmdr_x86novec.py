#Copyright 2022 Gonzalo Gómez-Sánchez & Aaron Call

#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

# Main script to apply MDR algorithm to epistatic dataset
# Removing numpy to make a more fair comparation with risk-

# IMPORTS
import time, pyspark, timeit, sys, getopt, random, os
import pandas as pd
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SparkSession
#from pyspark.sql.functions import *
from datetime import datetime
from collections import Counter

# PATHS - they are set to run in local
DATAPATH = "/home/ubuntu/Epistasia/riskv/sdata/input/"
OUTPUTPATH = "/home/ubuntu/Epistasia/riskv/sdata/output/"



####################
#### ARGUMENTS #####
####################

# Date
now = datetime.now()
time = now.strftime("%m%d%y%H%M")

# Default arguments
nf = 1

# Get arguments
arglist = sys.argv[1:]
 
# Options
options = "hf:c:w:"

# Long options
long_options = ["help", "files", "cores", "workers"]

try:
    # Parsing argument
    arguments, values = getopt.getopt(arglist, options, long_options)

    # Checking each argument
    for currentArgument, currentValue in arguments:

        # Help
        if currentArgument in ("-h", "--help"):
            print("Please indicate the following values. Otherwise default values will be used")
            print("-f, --files [int]: use this option followed by an integer to indicate the number of files to be processed.")
            sys.exit()

        # Set the number of variants to be processed
        elif currentArgument in ("-f", "--files"):
            nf  = int(currentValue)
            print(("Number of files to be processed %s.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-c", "--cores"):
            CORES = int(currentValue)
            print(("Number of partitions of the rdd set to %s.") % currentValue)

        # Set the number of partitions/slices of the rdd
        elif currentArgument in ("-w", "--workers"):
            WORKERS = int(currentValue)
            print(("Number of workers of spark set to %s.") % currentValue)


# Default values
except getopt.error as err:
    print("Number of files to process set to 1.")
    print("Number of partitions set to default.")


#####################
##### FUNCTIONS #####
#####################

# Definition of the filter for imputation. We believe the value only if > 0.9
def filter_imputation(x):
    if float(x) >0.9:
        return 1
    return 0

# Apply MDR to every SNP-SNP combination reading from a dict
def apply_mdr_dict(x, rd1, rd2):
    key1 = x[0]
    key2 = x[1]
    row1 = rd1[key1]
    row2 = rd2[key2]

    patients = transform_patients((row1,row2))
    
    testerror = get_risk_array(patients)

    return x[0], testerror

# Apply function to every SNP-SNP row to get the interaction matrix
def transform_patients(x):

    #1 - Get patients arrays
    patients1 = x[0]
    patients2 = x[1]

    # Create code list
    ptcode = list()

    #2 - Go over every patient (1 hot encoding of size 3)
    for i in range(len(patients1)):
        if i%3 == 0:
            p1 = patients1[i:i+3]
            p2 = patients2[i:i+3]

            #3 - Transform to integer code from 1 to 9
            code = (p1[0]*3+p1[1]*6+p1[2]*9) - (p2[0]*0 + p2[1]*1 + p2[2]*2)
            if code < 0:
                code = 0
            ptcode.append(code)

    return ptcode

# Count number of cases and number of controls for each column and return the high risk combinations.
# Then, use the high risk predictor to obtain the classification and prediction error.
def get_risk_array(patients):

    # Error list
    #trainerror = list()
    testpredpower = list()
    
    for i in range(5):
        
        # 1 - Get the sets for the iteration
        traindata = trainset[i]
        testdata = testset[i]
        Ntrain = sum(traindata)
        Ntest = sum(testdata)

        # 2 - Sum cases and controls from training set
        cases = list()
        controls = list()
        for i in range(len(patients)):

            # Cases
            if dtcases[i] == 1 and traindata[i] == 1:
                cases.append(patients[i])
            else:
                cases.append(0)

            # Controls
            if dtcontrols[i] == 1 and traindata[i] == 1:
                controls.append(patients[i])
            else:
                controls.append(0)

        sumcases = count_occurrences(cases)
        sumcontrols = count_occurrences(controls)

        # 3 - Get risk array
        risk = [div_risk(m,n) for m, n in zip(sumcases, sumcontrols)]

        # 4 - Transform to high risk = 1, low risk = 0
        risk = [get_risk(x, ccratio) for x in risk]

        # 5 - Classify training set
        prediction = [apply_risk(x, risk) for x in patients]
 
        # 6 - Get prediction power
        prederror = [1-(m+n)%2 for m, n in zip(prediction, dtcases)]
        cv_predpower = [m*n for m, n in zip(prederror, testdata)]
        testpredpower.append(sum(cv_predpower)/Ntest)

    return testpredpower

# Assign high risk or low risk 
def get_risk(x, ratio):
    if x >= ratio:
        return 1
    else:
        return 0

# Divide values and avoid division by 0
def div_risk(m,n):
    if n == 0:
        return 0
    else:
        return m/n

# Transform the counts to an array
def count_occurrences(data):
    
    dtcounts = Counter(data)
    aux = list()
    for i in range(10):
        if i in dtcounts:
            aux.append(dtcounts[i])
        else:
            aux.append(0)
            
    return aux

# Classify the patients with the risk vector
def apply_risk(x, risk):

    # Check for error in the data
    if x > 0:
        return risk[x-1]
    else:
        return 0

# Transform to key + values
def get_keyval(x):
    aux = x.split()
    key = aux[0] + '-'+ aux[1] +'-'+ aux[2]
    val = [filter_imputation(x) for x in aux[5:]]
    return (key, val)

# Combine two keys into one
def combine(x):
    k1 = x[0]
    k2 = x[1]
    if k1 == k2:
        return("NONE", 0)
    else:
        if k1 > k2:
            return(k1 + "_" + k2, 0)
        else:
            return(k2 + "_" + k1, 0)


################
##### MAIN #####
################

print("")
print("---------------------")
print("--- STARTING ---")
print("")

starttime0 = timeit.default_timer()

FOLDER = "partitions"
MASTER = 'bscdc18'
PORT = '7078'
nPart = CORES*WORKERS

spark = SparkSession \
        .builder \
        .master('spark://'+MASTER+':'+PORT) \
        .appName("MDRspark") \
        .getOrCreate()

sc = spark.sparkContext

print("Connected with spartk")

spconf = spark.sparkContext.getConf().getAll()

print("Connected with spark")
print(spark.sparkContext.getConf().getAll())

# Read patients information from HDFS
labels = spark.read.options(header = True, delimiter = " ").csv("file:///" + DATAPATH + "phs000237v1_eMERGE_T2D_NORTHWESTERN_1.sample")

print("Patients read")

# Keep only case/control information
for h in labels.columns:
    if h != "bin1":
        labels = labels.drop(h)

# Filter first non relevant row
labels = labels.filter("bin1!='B'")
labels = pd.DataFrame(labels.select("bin1").collect(), columns = ["bin1"])

# Get array with cases == 1
dtcases = [int(x) for x in labels["bin1"].astype(int).tolist()]

# Get array with controls == 1
dtcontrols = [(1 - x) for x in dtcases]

# Get cases/controls ratio. We will use the number as the high risk/low risk separator
ccratio = sum(dtcases)/sum(dtcontrols)
print("Ration cases/controls: " + str(ccratio))

# Create training and test set
Npatients = 1128
Ntrain = Npatients/5*4
Ntest = Npatients/5

# Create training and test set for a 5-CV 
Npatients = 1128
block_size = int(Npatients/5)

trainset = list()
testset = list()

# Create two list with the train and test indexes
for i in range(5):
    train = [1] * Npatients 
    train[i*block_size:i*block_size + block_size] = [0]*block_size
    test = [(1-x) for x in train]
    
    trainset.append(train)
    testset.append(test)

# Read files to be processed
files = sc.textFile("file:///" + DATAPATH + "/" + FOLDER + "/" + "listoffiles.txt").collect()

print("List of files obtained.")

# Number of files to process
n1 = 0
n2 = 0
total_pairs = 0

print("----- RUNNING " + str(nf) + " FILES -----")
print("------------------------------------------")

# Compute all the files against all the files
for f1 in files:
    
    print("Loading file " + f1 + " number " + str(n1))
    starttimef1 = timeit.default_timer()

    # Read file 1
    rdd1 = spark.sparkContext.textFile("file:///" + DATAPATH +"/" + FOLDER + "/" + f1)

    # Convert into key-val rdd
    #print("keyval1")
    rdd1 = rdd1.map(get_keyval)

    # Save data to HBASE
    rdd1dict = rdd1.collectAsMap()

    # Keep only the keys information
    rdd1 = rdd1.keys()

    # Parallelize dataset
    rdd1 = sc.parallelize(rdd1.collect(), nPart)

    # Compute against all the other files
    for f2 in files:

        #print("Processing secondary file " + f2)
        if f1 < f2:
            outfolder = "mdr_" + f1.replace(".gz","") + "_" + f2.replace(".gz","")
        else:
            outfolder = "mdr_" + f2.replace(".gz","") + "_" + f1.replace(".gz","")

        if os.path.isdir(OUTPUTPATH + outfolder):
            #print("Skipping file. " + outfolder  + " already processed.")
            #print("")
            n2+=1
            continue

        rdd2 = spark.sparkContext.textFile("file:///" + DATAPATH + "/" + FOLDER + "/" + f2)

        # Convert into key-val rdd
        rdd2 = rdd2.map(get_keyval)

        # Save data to HBASE
        rdd2dict = rdd2.collectAsMap()

        # Keep only the keys information
        print("keyval2")
        rdd2 = rdd2.keys()
        
        # Parallelize dataset
        rdd2 = sc.parallelize(rdd2.collect(), nPart)

        #print("File 2 loaded.")
        #print("Applying MDR...") 

        # Calculate execution time
        starttimerf2 = timeit.default_timer()

        # Create new rdd with tuples of every combination
        cartesiankeys = rdd1.cartesian(rdd2)

        # Compute MDR
        mdrerror = cartesiankeys.map(lambda x: apply_mdr_dict(x, rdd1dict, rdd2dict))
        #print("MDR applied to: " +  str(mdrerror.count()) + " pairs.")
        total_pairs += mdrerror.count()

        #print("Saving data...")
        mdrerror.saveAsTextFile("file:///" + OUTPUTPATH  + outfolder)
        #print("Data saved.")
        #print("")

        n2+=1
        if n2==nf:
            n2 = 0
            break

    print("Combined main file " + f1 + " in: " , timeit.default_timer()-starttimef1)
    print("------------------------------------------")
    print ("")

    n1+=1
    if n1==nf:
        break


print("Total time: " + str(timeit.default_timer()-starttime0))
print("Total pairs processed: " + str(total_pairs))

with open("logs/novec_c" + str(CORES) + "_w" + str(WORKERS) + ".log", 'a') as outfile:
    outfile.write("Number of cores: " + str(CORES) + "\n")
    outfile.write("Number of workers: " + str(WORKERS) + "\n")
    outfile.write("Dataset: " + FOLDER + "\n")
    outfile.write("Number of files: " + str(nf) + "\n")
    outfile.write("Total pairs processed: " + str(total_pairs)+ "\n")
    outfile.write("Total time: " + str(timeit.default_timer()-starttime0)+"\n")
    outfile.write("\n")
