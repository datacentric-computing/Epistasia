WORKERS=`wc --lines < workers.txt`

CORES=3

./start_spark.sh master.txt workers.txt $CORES
#./run_np_experiments.sh $CORES $WORKERS
./run_experiments.sh $CORES $WORKERS
./stop_spark.sh master.txt workers.txt

