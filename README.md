# Epistasia 

This is a testing repository. It is estructured as follows

# /data
Data folder with input data for testing (only chromosome 22 and patients information)
Download full dataset from bscdc cluster at path bscdc-login.bsc.es:/home/ggomez/epistasis/data/NuGENE_bychr_repaired

# /sdata
Data folder with a reduced dataset with 1000 lines for testing

# /main
Here you can find the full setup of MDR using Apache Spark and Hadoop File System.

#### run_all.sh 
This script will run the experiments desigend using the other scripts on the path. Check it for understanding how it works. You need to have your singularity on your home directory. Please update the main paths to your user to avoid errors.


# /singularity
Singularity definitions and utils to process the data using spark and/or hadoop with a singularity image

#### confcluster
Configuration files for hadoop and hbase to build the singularity for the bscdc cluster. Edit the paths before building the image.

#### conflocal
Configuration files for hadoop and hbase to build the singularity for local usage. Edit the paths before building the image.

#### defs
Definition files. Main file is currently ibsc_hbsp_mdr.def for cluster configuration.

#### images
Singularity images. Build and store your singularity images in here.

# /riskv
Here you can find a riskv prepared version for testing in a riskv enviroment

# /notebook
Here there are some notebook to visualize some results

# /utils
Some extra scripts.

# /chisquare
Tests to perform only chi-square test using spark and HDFS. Just testing for metrics.

# /mn
It includes scripts for testing the MDR in marenostrum. Currently it only uses spark.

#### commontesting
Scripts for processing all cohorts with the reduced dataset of common variants

#### toptesting
Scripts for processing only the top chi-square pairs. Working with NuGENE only.

#### testing
Other MN tests

# /spark
Python script to process the data using spark

